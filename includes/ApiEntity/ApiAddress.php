<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiAddress extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('addresses');
        parent::__construct($options, $id);
    }
}
