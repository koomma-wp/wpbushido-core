<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiExternalService extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('external_services');
        parent::__construct($options, $id);
    }
}
