<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity\Akeneo;

use WPBushidoCore\ApiEntity\ApiEntity;

class ApiAkeneoCategory extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('akeneo/categories');
        parent::__construct($options, $id);
    }

    public function get($params, $cache = array())
    {
        return parent::get(
            $params,
            [
                "ttl" => 3600,
                "prefix" => 'cachewpclient_api_akeneocategory'
            ]
        );
    }
}
