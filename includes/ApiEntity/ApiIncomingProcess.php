<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiIncomingProcess extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('incoming_processes');
        parent::__construct($options, $id);
    }
}
