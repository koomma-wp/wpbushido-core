<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiCredit extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('credits');
        parent::__construct($options, $id);
    }
}
