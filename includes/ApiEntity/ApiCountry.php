<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiCountry extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('countries');
        parent::__construct($options, $id);
    }
}
