<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiSale extends ApiEntity
{
    const STATUS_CREATED = 'created';
    const STATUS_WAITING_POINT_PAYMENT = 'waiting_point_payment';
    const STATUS_ERROR_POINT_PAYMENT = 'error_point_payment';
    const STATUS_REFUSED_POINT_PAYMENT = 'refused_point_payment';
    const STATUS_WAITING_CB_PAYMENT = 'waiting_cb_payment';
    const STATUS_ERROR_CB_PAYMENT = 'error_cb_payment';
    const STATUS_CANCELED = 'canceled';
    const STATUS_REFUSED_CB_PAYMENT = 'refused_cb_payment';
    const STATUS_CONFIRMED = 'confirmed';
    const STATUS_WAITING_PROCESS = 'waiting_treatment';
    const STATUS_ERROR_PROCESS = 'error_treatment';
    const STATUS_PROCESSED = 'processed';

    public function __construct($options, $id = false)
    {
        $this->setEntity('sales');
        parent::__construct($options, $id);
    }
}
