<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiMailReporting extends ApiEntity
{
    private $userToken = false;

    private $replaceContent = array('src' => array(), 'dst' => array());

    public function __construct($options, $id = false)
    {
        $this->setEntity('mail_reportings');
        parent::__construct($options, $id);
    }

    public function getUserToken()
    {
        return $this->userToken;
    }

    public function setUserToken($userToken)
    {
        $this->userToken = $userToken;

        return $this;
    }

    public function getReplaceContent()
    {
        return $this->replaceContent;
    }

    public function setReplaceContent($replaceContent)
    {
        $this->replaceContent = $replaceContent;

        return $this;
    }
}
