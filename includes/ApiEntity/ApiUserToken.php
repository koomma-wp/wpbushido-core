<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiUserToken extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('user_tokens');
        parent::__construct($options, $id);
    }
}
