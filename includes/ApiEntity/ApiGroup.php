<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiGroup extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('user_groups');
        parent::__construct($options, $id);
    }
}
