<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiExternalServiceGroup extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('external_service_groups');
        parent::__construct($options, $id);
    }
}
