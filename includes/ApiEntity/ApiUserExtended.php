<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiUserExtended extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('user_extendeds');
        parent::__construct($options, $id);
    }
}
