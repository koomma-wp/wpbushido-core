<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

use WPBushido\Helpers\DateTime;
use WPBushidoCore\ApiEntity\Akeneo\ApiAkeneoProduct;

class ApiCart extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('carts');
        parent::__construct($options, $id);
    }

    public function getDetails($context)
    {
        $cartItems = array();
        $cartItemsDetails = array();
        $cartItemsTotal = 0;
        if (isset($this->getData()['cartItems']) && count($this->getData()['cartItems']) > 0) {
            $cartItems = $this->getData()['cartItems'];
        }
        foreach ($cartItems as $cartItem) {
            $apiApiAkeneoProduct = new ApiAkeneoProduct($context, $cartItem['uri']);
            $product = $apiApiAkeneoProduct->get(array());
            if (!is_wp_error($product)) {
                $commandDate = new \DateTime();
                $deliveryDate = $commandDate->add(new \DateInterval('P15D'));
                $cartItemDetail = $product;
                $cartItemDetail['quantity'] = $cartItem['quantity'];
                $cartItemDetail['delivery_date'] = $deliveryDate->format(\DateTimeInterface::RFC3339);
                $cartItemDetail['total'] = $cartItem['quantity'] * $product['values']['price_point'];
                $cartItemsTotal += $cartItemDetail['total'];
                $cartItemsDetails[] = $cartItemDetail;
            }
        }
        return array('total' => $cartItemsTotal, 'details' => $cartItemsDetails);
    }


    public function countNbrItems()
    {
        $quantity = 0;
        foreach ($this->getData()['cartItems'] as $item) {
            $quantity += $item['quantity'];
        }

        return $quantity;
    }
}
