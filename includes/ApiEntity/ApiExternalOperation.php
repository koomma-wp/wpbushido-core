<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiExternalOperation extends ApiEntity
{
    private $allowedExtension = array();

    private $services = array();

    const STATUS_IN_PENDING = 'in_pending';
    const STATUS_CANCELED = 'canceled';
    const STATUS_VALIDATED = 'validated';
    const STATUS_TREATED = 'treated';


    public function __construct($options, $id = false)
    {
        $this->setEntity('external_operations');
        parent::__construct($options, $id);
    }

    public function getAllowedExtension()
    {
        return $this->allowedExtension;
    }

    public function setAllowedExtension($allowedExtension)
    {
        $this->allowedExtension = $allowedExtension;

        return $this;
    }

    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }

    public function getServices()
    {
        if (empty($this->services)) {
            $this->setServices($this->getCustom($this->getEntity().'/'.$this->getId().'/services', array()));
        }
        return $this->services;
    }
}
