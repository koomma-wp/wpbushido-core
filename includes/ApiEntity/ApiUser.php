<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiUser extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('users');
        parent::__construct($options, $id);
    }

    public function auth($params, $oauth = false)
    {
        $client = $this->getApiClient()->getFrontUserClient($params, $oauth);
        if (null === $client || is_wp_error($client)) {
            return $client;
        }

        $route = 'me';
        $response = $client->request('GET', $this->getApiClient()->getBaseUri().'/'.$route, [
            'json' => $params
        ]);

        return $response;
    }
}
