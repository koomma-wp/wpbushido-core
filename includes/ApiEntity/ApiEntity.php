<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

use WPBushidoCore\Api\ApiManager\ApiSymfony;

class ApiEntity
{
    const API_ROUTE = '/api/';

    private $apiClient;

    private $entity;

    private $id;

    private $data;

    public function __construct($options, $id = false)
    {
        $this->setApiClient(ApiSymfony::instance($options['options']['lang'], $options['options']['country'], get_current_blog_id()));
        if ($id) {
            $this->setId($id);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = str_replace(self::API_ROUTE.$this->getEntity().'/', '', $id);

        $this->id = $id;

        return $this;
    }

    public function getApiClient(): ?object
    {
        return $this->apiClient;
    }

    public function setApiClient(?object $apiClient): self
    {
        $this->apiClient = $apiClient;

        return $this;
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(?string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(?array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function get($params, $cache = array())
    {
        $return = $this->getApiClient()->getEntity($this->getId(), $this->getEntity(), $params, $cache);
        if (!is_wp_error($return)) {
            $this->setData($return);
        } else {
            $this->setData(array());
        }
        return $return;
    }

    public function search($params, $cache = array())
    {
        return $this->getApiClient()->getEntities($this->getEntity(), $params, $cache);
    }

    public function post($params)
    {
        return $this->getApiClient()->createEntity($this->getEntity(), $params);
    }

    public function postFile($params)
    {
        return $this->getApiClient()->createFileEntity($this->getId(), $this->getEntity(), $params);
    }

    public function put($params)
    {
        return $this->getApiClient()->updateEntity($this->getId(), $this->getEntity(), $params);
    }

    public function patch($params)
    {
        return $this->getApiClient()->updatePatchEntity($this->getId(), $this->getEntity(), $params);
    }

    public function getCustom($route, $params, $cache = array())
    {
        return $this->getApiClient()->getEntityWithCustomRoute($route, $params, $cache);
    }

    public function postCustom($route, $params)
    {
        return $this->getApiClient()->postEntityWithCustomRoute($route, $params);
    }

    public function delete()
    {
        return $this->getApiClient()->deleteEntity($this->getId(), $this->getEntity());
    }
}
