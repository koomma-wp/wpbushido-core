<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiProcess extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('processes');
        parent::__construct($options, $id);
    }
}
