<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiSponsoredUser extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('sponsored_users');
        parent::__construct($options, $id);
    }
}
