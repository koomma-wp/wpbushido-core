<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiRuleProcess extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('rule_processes');
        parent::__construct($options, $id);
    }
}
