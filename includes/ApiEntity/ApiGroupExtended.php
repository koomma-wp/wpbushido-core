<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiGroupExtended extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('user_group_extendeds');
        parent::__construct($options, $id);
    }
}
