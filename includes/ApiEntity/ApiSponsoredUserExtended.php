<?php

/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\ApiEntity;

class ApiSponsoredUserExtended extends ApiEntity
{
    public function __construct($options, $id = false)
    {
        $this->setEntity('sponsored_user_extendeds');
        parent::__construct($options, $id);
    }
}
