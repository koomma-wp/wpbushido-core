<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Log;

use Inpsyde\Wonolog\Data\Log as Wonolog;

class Log
{
    public static function setLog($log, $type = false)
    {
        if (is_wp_error($log)) {
            foreach ($log->get_error_messages() as $error) {
                if ($error instanceof \GuzzleHttp\Psr7\Response) {
                    $log = $error->getBody()->getContents();
                } else {
                    $log = error;
                }
                do_action(
                    'wonolog.log',
                    $log,
                    constant("Monolog\Logger::ERROR")
                );
            }

        } else {
            do_action(
                'wonolog.log',
                new Wonolog(
                    $log,
                    constant("Monolog\Logger::$type")
                )
            );
        }
    }
}