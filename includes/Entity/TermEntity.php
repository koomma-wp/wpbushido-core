<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use \Timber\Term as Term;

class TermEntity extends Entity
{

    private $term;

    private $taxonomy = '';

    public function __construct($id, $taxonomy = '')
    {
        parent::__construct($id);
        $this->setTaxonomy($taxonomy);
        $this->wpGetTerm();
    }

    /**
     * Get Wordpress Timber/Term Object
     *
     * @return mixed
     */
    public function getTerm(): ?object
    {
        return $this->term;
    }

    /**
     * Set Wordpress Timber/Term Object
     *
     * @param object term
     * @return mixed
     */
    public function setTerm(?object $term): self
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get Taxonomy
     *
     * @return string
     */
    public function getTaxonomy(): ?string
    {
        return $this->taxonomy;
    }

    /**
     * Set Taxonomy
     *
     * @param string $taxonomy
     * @return mixed
     */
    public function setTaxonomy(?string $taxonomy): self
    {
        $this->taxonomy = $taxonomy;

        return $this;
    }

    /**
     * Set Timber from Term
     *
     * @return void
     */
    public function wpGetTerm()
    {
        $timberTerm = new Term($this->getId(), $this->getTaxonomy());
        if (isset($timberTerm->name) && !empty($timberTerm->name)) {
            $this->setTerm($timberTerm);
            $this->setIsRestored(true);
        } else {
            $this->setTerm(false);
            $this->setIsRestored(false);
        }
    }
}
