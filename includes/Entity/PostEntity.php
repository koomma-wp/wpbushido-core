<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use \Timber\Post as Post;

class PostEntity extends Entity
{

    private $post;

    public function __construct($id)
    {
        parent::__construct($id);
        $this->wpGetPost();
    }

    /**
     * Get Wordpress post
     *
     * @return object wordpress post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set Wordpress post
     *
     * @param mixed wordpress post
     * @return mixed
     */
    public function setPost($post)
    {
        $this->post = $post;
        return $this;
    }

    /**
     * Set Timber post
     *
     * @return void
     */
    public function wpGetPost()
    {
        $timberPost = new Post($this->getId());
        if (isset($timberPost->id) && !empty($timberPost->id)) {
            $this->setPost($timberPost);
            $this->setIsRestored(true);
        } else {
            $this->setPost(false);
            $this->setIsRestored(false);
        }
    }
}
