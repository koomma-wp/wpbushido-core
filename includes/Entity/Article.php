<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use WPBushidoCore\Entity\PostEntity;

class Article extends PostEntity
{

    private $categories;

    public function __construct($id)
    {
        parent::__construct($id);
        $this->setCategories();
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories()
    {
        $return = array();
        if ($this->getRestored()) {
            foreach ($this->getPost()->categories() as $category) {
                if ($category->taxonomy == 'category' && $category->slug != 'uncategorized') {
                    if ($category->parent > 0) {
                        $returnParentObj = new TermEntity($category->parent, 'category');
                        if ($returnParentObj->getRestored()) {
                            if (!isset($return[$returnParentObj->getTerm()->slug])) {
                                $return[$returnParentObj->getTerm()->slug] = array();
                            }
                            $returnObj = new TermEntity($category->id, 'category');
                            if ($returnObj->getRestored()) {
                                $return[$returnParentObj->getTerm()->slug][] = $returnObj->getTerm();
                            }
                        }
                    }
                }
            }
        }
        $this->categories = $return;
    }
}
