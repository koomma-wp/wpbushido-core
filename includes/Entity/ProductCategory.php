<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use WPBushidoCore\Entity\PostEntity;

class ProductCategory extends PostEntity
{

    private $akeneocategories = array();

    public function __construct($id)
    {
        parent::__construct($id);
    }

    public function setAkeneoCategories($akeneocategories)
    {
        if (empty($this->akeneocategories)) {
            $akeneocategoriesforce = array();
            if (is_array($this->getPost()->meta('akeneo_pim_categories'))) {
                foreach ($this->getPost()->meta('akeneo_pim_categories') as $akeneocategory) {
                    $slug = str_replace('/api/akeneo/categories/', '', $akeneocategory);
                    $akeneocategoriesforce[$slug] = $akeneocategory;
                }
            }
            $this->akeneocategories = $akeneocategoriesforce;
        } else {
            $this->akeneocategories = $akeneocategories;
        }
        return $this;
    }

    public function getAkeneoCategories()
    {
        return $this->akeneocategories;
    }
}
