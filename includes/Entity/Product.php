<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use WPBushidoCore\Entity\PostEntity;

class Product extends PostEntity
{
    public function __construct($id)
    {
        parent::__construct($id);
    }
}
