<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use WPBushidoCore\ApiEntity\ApiSponsoredUser;
use WPBushidoCore\Log\Log;

class SponsoredUser extends Entity
{

    private $apiSponsoredUser = false;

    private $extendedFields = array();

    private $fields = array(
        'lastName' => true,
        'firstName' => true,
        'email' => true,
        'civility' => false
    );

    public function __construct($id)
    {
        parent::__construct($id);
    }

    /**
     * Set fields definitions
     *
     * @param array $fields
     * @return object
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Get fields definitions
     *
     * @return array fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields definitions
     *
     * @param array $extendedFields
     * @return object
     */
    public function setExtendedFields($extendedFields)
    {
        $this->extendedFields = $extendedFields;
        return $this;
    }

    /**
     * Get fields definitions
     *
     * @return array fields
     */
    public function getExtendedFields()
    {
        return $this->extendedFields;
    }

    public function getApiSponsoredUser()
    {
        if (!$this->apiSponsoredUser && $this->getId()) {
            $apiSponsoredUser = new ApiSponsoredUser($this->getContext(), $this->getId());
            $returnApi = $apiSponsoredUser->get(array());
            if (!is_wp_error($returnApi)) {
                $apiSponsoredUser->setData($returnApi);
                $this->setApiSponsoredUser($apiSponsoredUser);
                $this->getApiSponsoredUser()->setData($returnApi);
            }
        }
        return $this->apiSponsoredUser;
    }

    public function setApiSponsoredUser($apiSponsoredUser)
    {
        $this->apiSponsoredUser = $apiSponsoredUser;
        return $this;
    }
}
