<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

class Entity
{

    private $id;

    private $isRestored;

    private $context;


    public function __construct($id)
    {
        $this->setId($id);
    }

    /**
     * Get Id
     *
     * @return mixed id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Id
     *
     * @param mixed id
     * @return mixed
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }


    /**
     * Get isRestored
     *
     * @return boolean isRestored
     */
    public function getRestored(): ?bool
    {
        return $this->isRestored;
    }

    /**
     * Set isRestored
     *
     * @param boolean $isRestored
     * @return mixed id
     */
    public function setIsRestored(?bool $isRestored): self
    {
        $this->isRestored = $isRestored;

        return $this;
    }

    /**
     * Set context (from Timber) property
     *
     * @param array $context
     * @return object
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Get context (from Timber) property
     *
     * @return array context
     */
    public function getContext()
    {
        return $this->context;
    }
}
