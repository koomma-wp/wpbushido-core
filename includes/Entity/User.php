<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Entity;

use \Timber\User as TimberUser;
use WPBushidoCore\ApiEntity\ApiAddress;
use WPBushidoCore\ApiEntity\ApiCart;
use WPBushidoCore\ApiEntity\ApiCountry;
use WPBushidoCore\ApiEntity\ApiSponsoredUser;
use WPBushidoCore\ApiEntity\ApiUserExtended;
use WPBushidoCore\ApiEntity\ApiGroupExtended;
use WPBushidoCore\ApiEntity\ApiGroup;
use WPBushidoCore\ApiEntity\ApiSale;
use WPBushidoCore\ApiEntity\ApiUser;
use WPBushidoCore\ApiEntity\ApiUserToken;
use WPBushidoCore\Log\Log;

class User extends Entity
{
    public const CART_SESSION = 'userApiCart';

    private $wpUser = false;

    private $apiUser = false;

    private $fields = array(
        'lastName' => true,
        'firstName' => true,
        'username' => true,
        'plainPassword' => false,
        'civility' => false
    );

    private $extendedFields = array();

    private $addresses = false;

    private $sponsoredUsers = false;

    private $addressFields = array(
        'name' => true,
        'type' => false,
        'civility' => true,
        'lastName' => true,
        'firstName' => true,
        'address' => true,
        'address2' => false,
        'zipCode' => true,
        'city' => true,
        'email' => true,
        'phone1' => true,
        'phone2' => false,
    );

    private $apiCart = false;

    private $apiSales = false;

    public function __construct($id)
    {
        parent::__construct($id);
    }

    public function getWpUser()
    {
        return $this->wpUser;
    }

    public function setWpUser($wpUser): self
    {
        $this->wpUser = $wpUser;

        return $this;
    }

    public function wpGetUser()
    {
        $timberUser = new TimberUser($this->getId());
        if (isset($timberUser->id) && !empty($timberUser->id)) {
            $this->setWpUser($timberUser);
            $this->setIsRestored(true);
        } else {
            $this->setWpUser(false);
            $this->setIsRestored(false);
        }
    }

    public function getApiUser()
    {
        if (!$this->apiUser && $this->getWpUser()) {
            $apiUser = new ApiUser($this->getContext(), $this->getWpUser()->display_name);
            $returnApi = $apiUser->get(array());
            if (!is_wp_error($returnApi)) {
                $apiUser->setData($returnApi);
                $this->setApiUser($apiUser);
                $this->getApiUser()->setData($returnApi);
            }
        }
        return $this->apiUser;
    }

    public function getApiUserFromId($id)
    {
        if (!$this->apiUser) {
            $apiUser = new ApiUser($this->getContext(), $id);
            $returnApi = $apiUser->get(array());
            if (!is_wp_error($returnApi)) {
                $apiUser->setData($returnApi);
                $this->setApiUser($apiUser);
                $this->getApiUser()->setData($returnApi);
            }
        }
        return $this->apiUser;
    }

    public function setApiUser($apiUser)
    {
        $this->apiUser = $apiUser;
        return $this;
    }

    public function getApiAuthUser($params)
    {
        $apiUser = new ApiUser($this->getContext());
        return $apiUser->auth($params);
    }

    /**
     * Set fields definitions
     *
     * @param array $fields
     * @return object
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        return $this;
    }

    /**
     * Get fields definitions
     *
     * @return array fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set Api Cart
     *
     * @param object $apiCart
     * @return object
     */
    public function setApiCart($apiCart)
    {
        $this->apiCart = $apiCart;
        if ($apiCart instanceof ApiCart) {
            $_SESSION[self::CART_SESSION] = serialize($this->apiCart->getData());
        }

        return $this;
    }

    public function clearApiCart()
    {
        $this->apiCart = false;
        if (isset($_SESSION[self::CART_SESSION])) {
            unset($_SESSION[self::CART_SESSION]);
        }

        return $this;
    }

    /**
     * Get Api cart
     *
     * @return mixed $apiCart
     */
    public function getApiCart($forceReload = false)
    {
        if (!$this->apiCart || $forceReload) {
            if (isset($_SESSION[self::CART_SESSION]) && !$forceReload) {
                $apiCart = new ApiCart($this->getContext());
                $apiCart->setData(unserialize($_SESSION[self::CART_SESSION]));
                $apiCart->setId($apiCart->getData()['@id']);
                $this->setApiCart($apiCart);
            } else {
                if ($this->getApiUser() instanceof ApiUser) {
                    $apiCart = new ApiCart($this->getContext());
                    $routeCart = $this->getApiUser()->getEntity() . '/' . $this->getApiUser()->getId() . '/' . str_replace('s', '', $apiCart->getEntity());
                    $apiCarts = $this->getApiUser()->getCustom($routeCart, array());
                    if (!is_wp_error($apiCarts)) {
                        $apiCart->setData($apiCarts);
                        $apiCart->setId($apiCart->getData()['@id']);
                        $this->setApiCart($apiCart);
                    } elseif (404 === $apiCarts->get_error_message()->getStatusCode()) {
                        $this->clearApiCart();
                    } elseif ($apiCarts->get_error_message()->getStatusCode() != 404) {
                        Log::setLog($apiCarts);
                    }
                }
            }
        }
        return $this->apiCart;
    }

    /**
     * Set Api Sales.
     *
     * @param object $apiSales
     *
     * @return object
     */
    public function setApiSales($apiSales)
    {
        $this->apiSales = $apiSales;

        return $this;
    }

    /**
     * Get Api sales.
     *
     * @return mixed $apiSales
     */
    public function getApiSales()
    {
        if (!$this->apiSales) {
            if ($this->getApiUser() instanceof ApiUser) {
                $apiSale = new ApiSale($this->getContext());
                $routeCart = $this->getApiUser()->getEntity().'/'.$this->getApiUser()->getId().'/'.$apiSale->getEntity();
                $apiSales = $this->getApiUser()->getCustom($routeCart, array());
                if (!is_wp_error($apiSales) && isset($apiSales['hydra:member'])) {
                    $this->setApiSales($apiSales['hydra:member']);
                } else {
                    Log::setLog($apiSales);
                }
            }
        }

        return $this->apiSales;
    }

    public function createApiCart()
    {
        if (!$this->apiCart) {
            $apiCart = new ApiCart($this->getContext());
            $postCart = $apiCart->post(
                array(
                    'user' => $this->getApiUser()->getData()['@id']
                )
            );
            if (!is_wp_error($postCart)) {
                $apiCart->setData($postCart);
                $apiCart->setId($apiCart->getData()['@id']);
                $this->setApiCart($apiCart);
            } else {
                Log::setLog($postCart);
            }
        }
    }

    public function updateApiCart($product, $quantity, $mode)
    {
        $cart = $this->getApiCart();
        if (!$cart) {
            $this->createApiCart();
        }
        $cartItems = array();
        if (isset($this->getApiCart()->getData()['cartItems'])
            && count($this->getApiCart()->getData()['cartItems']) > 0
        ) {
            $cartItems = $this->getApiCart()->getData()['cartItems'];
        }
        $existingProduct = false;
        foreach ($cartItems as $key => $item) {
            if ($item['uri'] == $product['@id']) {
                $existingProduct = true;
                switch ($mode) {
                    case 'change':
                        $cartItems[$key]['quantity'] = $quantity;
                        break;
                    case 'delete':
                        $cartItems[$key]['quantity'] = 0;
                        break;
                    case 'substract':
                        $cartItems[$key]['quantity'] -= $quantity;
                        break;
                    case 'add':
                    default:
                        $cartItems[$key]['quantity'] += $quantity;
                        break;
                }

                if ($cartItems[$key]['quantity'] <= 0) {
                    unset($cartItems[$key]);
                }
            }
        }
        if (!$existingProduct && 'add' === $mode && $quantity > 0) {
            $cartItems[] = [
                'uri' => $product['@id'],
                'quantity' => $quantity,
            ];
        }

        $putCart = array(
            'cartItems' => $cartItems
        );
        $patchCart = $this->getApiCart()->put($putCart);

        if (isset($patchCart->errors['api_apisymfony-wp-client'][0])
            && 404 === $patchCart->errors['api_apisymfony-wp-client'][0]->getStatusCode()) {
            $this->clearApiCart();
            $this->updateApiCart($product, $quantity, $mode);
        } else {
            Log::setLog($patchCart);
        }

        $this->getApiCart()->setData($patchCart);
        $this->setApiCart($this->getApiCart());
    }

    public function getApiCartDetails()
    {
        $cartDetails = $this->getApiCart()->getDetails($this->getContext());
        $goToCheckout = false;
        $needPayment = $cartDetails['total'] - $this->getApiUser()->getData()['points']['balance'];
        if ($cartDetails['total'] > 0 && $needPayment <= 0) {
            $goToCheckout = true;
        }
        $cartDetails['gocheckout'] = $goToCheckout;
        $cartDetails['needpayment'] = $needPayment;

        return $cartDetails;
    }

    /**
     * Set fields definitions
     *
     * @param array $extendedFields
     * @return object
     */
    public function setExtendedFields($extendedFields)
    {
        $this->extendedFields = $extendedFields;
        return $this;
    }

    /**
     * Get fields definitions
     *
     * @return array fields
     */
    public function getExtendedFields()
    {
        return $this->extendedFields;
    }

    /**
     * Set address fields definitions
     *
     * @param array $addressFields
     * @return object
     */
    public function setAddressFields($addressFields)
    {
        $this->addressFields = $addressFields;
        return $this;
    }

    /**
     * Get address fields definitions
     *
     * @return array fields
     */
    public function getAddressFields()
    {
        return $this->addressFields;
    }

    /**
     * Get lock sale status.
     *
     * @return bool lockSale
     */
    public function getLockSale()
    {
        return $this->getApiUser()->getData()['saleInError'];
    }

    /**
     * Get all user addresses
     *
     * @return array addresses
     */
    public function getAddresses()
    {
        $addresses = array('addresses' => array(), 'delivery' => array(), 'billing' => array());
        if (!$this->addresses) {
            $apiAddress = new ApiAddress($this->getContext());
            if ($this->getApiUser()) {
                $apiAddresses = $this->getApiUser()->getCustom($this->getApiUser()->getEntity() . '/' . $this->getApiUser()->getId() . '/' . $apiAddress->getEntity(), array());
                if (!is_wp_error($apiAddresses) && isset($apiAddresses['hydra:member'])) {
                    $addresses['addresses'] = $apiAddresses['hydra:member'];
                }
                foreach ($addresses['addresses'] as $address) {
                    if ($address['type'] == 3) {
                        $addresses['delivery'] = $address;
                        $addresses['billing'] = $address;
                    } else if ($address['type'] == 2) {
                        $addresses['delivery'] = $address;
                    } else if ($address['type'] == 1) {
                        $addresses['billing'] = $address;
                    }
                }
                $this->setAddresses($addresses);
            }
        }
        return $this->addresses;
    }

    /**
     * Set user addresses
     *
     * @param array $addresses
     * @return object
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
        return $this;
    }

    /**
     * Set user sponsored users
     *
     * @param array $sponsoredUsers
     * @return object
     */
    public function setSponsoredUsers($sponsoredUsers)
    {
        $this->sponsoredUsers = $sponsoredUsers;
        return $this;
    }

    /**
     * Get current User Sponsored Users
     *
     * @return array $sponsoredusers
     */
    public function getSponsoredUsers()
    {
        if (!$this->sponsoredUsers) {
            $context = $this->getContext();
            $apiSponsoredUser = new ApiSponsoredUser($context);
            $customRoute = $this->getApiUser()->getEntity() . '/' . $this->getApiUser()->getId() . '/' . $apiSponsoredUser->getEntity();
            $apiSponsoredUsers = $this->getApiUser()->getCustom($customRoute, array());
            if (!is_wp_error($apiSponsoredUsers) && isset($apiSponsoredUsers['hydra:member'])) {
                $sponsoredusers = $apiSponsoredUsers['hydra:member'];
            } else {
                $sponsoredusers = array();
            }
            $this->setSponsoredUsers = $sponsoredusers;
            return $sponsoredusers;
        } else {
            return $this->sponsoredUsers;
        }
    }

    /**
     * Check if user is logged and link to the API
     *
     * @return boolean
     */
    public function isLogged()
    {
        if (isset($this->getWpUser()->ID) && $this->getWpUser()->ID == wp_get_current_user()->ID) {
            $noApi = true;
            if ($this->getApiUser() instanceof ApiUser && isset($this->getApiUser()->getData()['@id'])) {
                $noApi = false;
            }
            if ($noApi) {
                $this->setId($this->getWpUser()->ID);
                if ($this->getApiUser() instanceof ApiUser && isset($this->getApiUser()->getData()['@id'])) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }

        } else {
            return false;
        }
    }

    /**
     * Try to login username/password on the API
     *
     * @param string $username
     * @param string $password
     * @return boolean
     */
    public function login($username, $password)
    {
        $checkLogin = array('username' => $username, 'password' => $password);
        $auth = $this->getApiAuthUser($checkLogin);
        if (!is_wp_error($auth) && isset($auth['@id']) && !empty($auth['@id'])) {
            $this->authByApiUser($auth);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Complete Logout the user
     *
     * @return void
     */
    public function logout()
    {
        session_destroy();
        wp_logout();
        $this->setApiUser(false);
        $this->setWpUser(false);
    }

    /**
     * Auth user by Api incoming data
     *
     * @param string $apiUser
     * @param boolean $nocookies (to disable session and cookie set - Wordpress side)
     * @return void
     */
    public function authByApiUser($apiUser, $nocookies = false)
    {
        $this->setApiUser(new ApiUser($this->getContext(), $apiUser['@id']));
        $this->getApiUser()->setData($apiUser);
        $userobj = new \WP_User();
        $loginId = strtolower(str_replace('/', '', $this->getApiUser()->getData()['@id']));
        $user = $userobj->get_data_by('login', $loginId);
        if (!$user || $user->ID == 0) {
            $userdata = array(
                'user_email' => sha1($this->getApiUser()->getData()['@id']).'@koomma.fr',
                'user_login' => $this->getApiUser()->getData()['@id'],
                'display_name' => $this->getApiUser()->getData()['@id'],
            );
            $new_user_id = wp_insert_user($userdata);
            $user = new \WP_User($new_user_id);
        } else {
            $user = new \WP_User($user->ID);
        }
        $this->setId($user->ID);
        $this->setWpUser($user);
        if (!$nocookies) {
            wp_set_current_user($user->ID);
            wp_set_auth_cookie($user->ID);
        }
    }

    /**
     * Get user by token
     *
     * @param mixed $token
     * @param string $name (the token name / type)
     * @return boolean
     */
    public function getUserToken($token = false, $name)
    {
        $apiUserToken = new ApiUserToken($this->getContext(), false);
        if ($token) {
            $user = $apiUserToken->getCustom($apiUserToken->getEntity() . '/' . $name . '/' . $token . '/check', array());
            if (!is_wp_error($user) && isset($user['@id']) && !empty($user['@id'])) {
                $this->setApiUser(new ApiUser($this->getContext(), $user['@id']));
                $this->getApiUser()->setData($user);
                return true;
            } else {
                return false;
            }
        } else {
            if (!empty($this->getApiUser()->getId())) {
                $tokens = $apiUserToken->getCustom($this->getApiUser()->getEntity().'/'.$this->getApiUser()->getId().'/'.$apiUserToken->getEntity(), array('name' => $name));
                if (!is_wp_error($tokens) && isset($tokens['hydra:member'][0]) && !empty($tokens['hydra:member'][0])) {
                    return $tokens['hydra:member'][0]['token'];
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }

    /**
     * Request reset password
     *
     * @param string $email
     * @return boolean
     */
    public function requestResetPassword($email)
    {
        if ($this->getApiUser() instanceof ApiUser) {
            $route = $this->getApiUser()->getEntity() . '/reset_password_request';
            return $this->getApiUser()->postCustom($route, array('username' => $email));
        } else {
            return false;
        }
    }

    /**
     * Complete User delete (api and wordpress)
     *
     * @return void
     */
    public function delete()
    {
        if ($this->getApiUser() instanceof ApiUser) {
            $this->getApiUser()->delete();
        }
        if ($this->getWpUser() instanceof TimberUser && isset($this->getWpUser()->ID)) {
            wp_delete_user($this->getWpUser()->ID);
        }
    }

    /**
     * Save multi action for user addresses : create, update or delete
     *
     * @return mixed
     */
    public function saveAddress()
    {
        $context = $this->getContext();
        $return = false;
        if (isset($context['incoming_args']['useraddress']) && !empty($context['incoming_args']['useraddress'])) {
            $apiAddress = new ApiAddress($context, $context['incoming_args']['useraddress']);
            $address = $apiAddress->get(array());
            if (!is_wp_error($address) && $address['user'] == $this->getApiUser()->getData()['@id']) {
                if (isset($context['incoming_args']['goDelete']) && !empty($context['incoming_args']['goDelete'])) {
                    $deleteAddress = $apiAddress->delete();
                    if (!is_wp_error($deleteAddress)) {
                        $return = $deleteAddress;
                    } else {
                        Log::setLog($deleteAddress);
                    }
                } else {
                    if (count($this->prepareUserAddress(true)['error']) == 0 && count($this->prepareUserAddress(true)['success']) > 0) {
                        $dataUpdate = $this->prepareUserAddress(true)['success'];
                        if ($dataUpdate['type'] == 2 && isset($this->getAddresses()['billing']) && $this->getAddresses()['billing']['@id'] == $context['incoming_args']['useraddress']) {
                            $dataUpdate['type'] = 3;
                        }
                        if ($dataUpdate['type'] == 1 && isset($this->getAddresses()['delivery']) && $this->getAddresses()['delivery']['@id'] == $context['incoming_args']['useraddress']) {
                            $dataUpdate['type'] = 3;
                        }
                        $putAddress = $apiAddress->put($dataUpdate);
                        if (!is_wp_error($putAddress)) {
                            $return = $putAddress;
                        } else {
                            Log::setLog($putAddress);
                        }
                    }
                }
            } else {
                Log::setLog($address);
            }
        } else {
            $apiAddress = new ApiAddress($context);
            if (count($this->prepareUserAddress(false)['error']) == 0 && count($this->prepareUserAddress(false)['success']) > 0) {
                $dataAddress = $this->prepareUserAddress(false)['success'];
                $dataAddress['user'] = $this->getApiUser()->getData()['@id'];
                $apiCountry = new ApiCountry($context, strtolower($context['options']['country']));
                $country = $apiCountry->get(array());
                if (!is_wp_error($country)) {
                    $dataAddress['country'] = $country['@id'];
                    $postAddress = $apiAddress->post($dataAddress);
                    if (!is_wp_error($postAddress)) {
                        $return = $postAddress;
                    } else {
                        Log::setLog($postAddress);
                    }
                } else {
                    Log::setLog($country);
                }

            }
        }
        $this->setAddresses(false);
        $this->getAddresses();
        return $return;
    }

    /**
     * Prepare user address before save
     *
     * @param boolean $update
     * @param array $forceParams
     * @return array $return
     */
    public function prepareUserAddress($update=false, $forceParams=array())
    {
        $context = $this->getContext();
        if (count($forceParams) > 0) {
            $addressParams = $forceParams;
        } else {
            $addressParams = $context['incoming_args'];
        }
        $return = array('success' => array(), 'error' => array());
        foreach ($this->getAddressFields() as $field => $definition) {
            if (isset($addressParams[$field])) {
                $value = $addressParams[$field];
                if ($value == 'on' || $value == 'yes') {
                    if (strstr($field, 'optin')) {
                        $dateOptin = new \DateTime();
                        $value = $dateOptin->format(\DateTime::RFC3339);
                    } else {
                        $value = true;
                    }
                }
                else if ($value == 'off' || $value == 'no' || $value == false) {
                    if (strstr($field, 'optin')) {
                        $value = null;
                    }
                }
                if ($field == 'type') {
                    $value = intval($value);
                }
                $return['success'][$field] = $value;
            } else {
                if ($definition && !$update) {
                    $return['error'][] = $field;
                }
            }
        }
        return $return;
    }

    /**
     * Save multi action for user sponsored user : create, update or delete
     *
     * @return mixed
     */
    public function saveSponsoredUser()
    {
        $context = $this->getContext();
        $return = false;
        if (isset($context['incoming_args']['sponsoreduser']) && !empty($context['incoming_args']['sponsoreduser'])) {
            $apiSponsoredUser = new ApiSponsoredUser($context, $context['incoming_args']['sponsoreduser']);
            $sponsoredUser = $apiSponsoredUser->get(array());
            if (!is_wp_error($sponsoredUser) && $sponsoredUser['sponsor'] == $this->getApiUser()->getData()['@id']) {
                if (isset($context['incoming_args']['goDelete']) && !empty($context['incoming_args']['goDelete'])) {
                    $deleteSponsoredUser = $apiSponsoredUser->delete();
                    if (!is_wp_error($deleteSponsoredUser)) {
                        $return = $deleteSponsoredUser;
                    } else {
                        Log::setLog($deleteSponsoredUser);
                    }
                } else {
                    $return = $sponsoredUser;
                }
            } else {
                Log::setLog($sponsoredUser);
            }
        } else {

        }
        return $return;
    }
}
