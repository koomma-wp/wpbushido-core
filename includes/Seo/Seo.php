<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Seo;

class Seo
{
    private $context = array();

    /**
     *
     * @var $checkSeoFields
     */
    private $checkSeoFields = array('title', 'description', 'keywords', 'image', 'main_title');

    public function __construct(array $context)
    {
        $this->setContext($context);
    }

    /**
     * Set context
     *
     * @param mixed $context
     * @return object Seo
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Get context
     *
     * @return mixed context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Get Seo information
     *
     * @param int $forceParent
     * @return array $return all seo information
     */
    public function getSeo($forceParent = 0)
    {
        $context = $this->getContext();
        if (!isset($context['page_seo'])) {
            $return = array();
        }
        else {
            $return = $context['page_seo'];
        }
        $pageId = false;
        if ($forceParent) {
            $pageId = $forceParent;
        }
        else if (isset($context['page_id']) && !empty($context['page_id'])) {
            $pageId = $context['page_id'];
        }
        if ($pageId) {
            $acfFields = get_fields($pageId);
            if (isset($acfFields['seo_parent_synchronisation']) && $acfFields['seo_parent_synchronisation'] == true && $pageId != intval(get_option('page_on_front'))) {
                $parentID = wp_get_post_parent_id($pageId);
                if ($parentID == 0) {
                    $parentID = intval(get_option('page_on_front'));
                }
                $return = $this->getSeo($parentID);
            }
            else {
                foreach ($this->checkSeoFields as $field) {
                    if (isset($acfFields['seo_' . $field]) && !empty($acfFields['seo_' . $field])) {
                        $return['seo_' . $field] = $this->checkSeoString($acfFields['seo_' . $field]);
                    }
                    if (isset($acfFields['seo_' . $field . '_detail']) && !empty($acfFields['seo_' . $field . '_detail'])) {
                        $return['seo_' . $field . '_detail'] = $this->checkSeoString($acfFields['seo_' . $field . '_detail']);
                    }
                    if (isset($acfFields['seo_' . $field . '_form']) && !empty($acfFields['seo_' . $field . '_form'])) {
                        $return['seo_' . $field . '_form'] = $this->checkSeoString($acfFields['seo_' . $field . '_form']);
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Process on SEO data
     *
     * @param string $string : string SEO to proceed
     * @return string $string string with all changes
     */
    public function checkSeoString($string)
    {
        $context = $this->getContext();
        $arrayStopWords = array(
            'item'
        );
        $findStepWordsExpression = array();
        preg_match_all('(\#[^#]+\#)', $string, $matches);
        if (isset($matches[0])) {
            $matches = $matches[0];
        }
        foreach ($matches as $match) {
            $cleanStepWords = $match;
            $cleanStepClean = '';
            preg_match('(\(.*\))', $match, $matchesInto);
            foreach ($matchesInto as $matchesIntoItem) {
                $cleanStepWords = str_replace(array($matchesIntoItem, '#'), array('',''), $match);
                $cleanStepClean = str_replace(array('(',')'), array('', ''), $matchesIntoItem);
            }
            $findStepWordsExpression[] = array('stepword' => $cleanStepWords, 'expression' => $cleanStepClean, 'original' => $match);
        }
        foreach ($findStepWordsExpression as $stepWordK) {
            $stepWord = $stepWordK['stepword'];
            $expressionWord = $stepWordK['expression'];
            $originalWords = $stepWordK['original'];
            $replaceValue = '';
            if (in_array($stepWord, $arrayStopWords)) {
                $value = '';
                if (!empty($value)) {
                    if (!empty($expressionWord)) {
                        $replaceValue = str_replace('%s', $value, $expressionWord);
                    } else {
                        $replaceValue = $value;
                    }
                }
            }
            $string = str_replace($originalWords, $replaceValue, $string);
        }
        $string = trim($string);
        $string = str_replace('  ', ' ', $string);
        return $string;
    }

}