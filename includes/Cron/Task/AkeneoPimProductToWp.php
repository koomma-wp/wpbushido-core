<?php
/**
 * Task extension for WP-Command
 *
 * @package WPPI
 */

namespace WPBushidoCore\Cron\Task;

use WPBushido\Cron\Task;
use WPBushidoCore\Repository\Akeneo\ApiAkeneoProductRepository;
use WPBushidoCore\Repository\ProductCategoryRepository;
use WPBushidoCore\Repository\ProductRepository;

class AkeneoPimProductToWp extends Task
{
    const NAME = 'akeneopimproduct_to_wp';

    public static function registerHooks()
    {
        add_action('wpcmd__'. self::NAME, array('WPBushidoCore\Cron\Task\AkeneoPimProductToWp', 'run'));
        add_filter('wpcmdopt__'. self::NAME, array('WPBushidoCore\Cron\Task\AkeneoPimProductToWp', 'getOptions'));
    }

    public static function run($arguments = array())
    {
        // php wp-command.php --task=akeneopimproduct_to_wp --host=https://le-cercle-club-local.bushido-factory.io
        // php wp-command.php --task=akeneopimproduct_to_wp --host=https://le-cercle-club-preprod.bushido-factory.io
        // php wp-command.php --task=akeneopimproduct_to_wp --host=https://merci-mon-cercle.fr
        return self::execute('akeneopimproduct_to_wp', $arguments);
    }

    public static function getOptions()
    {
        return array('index:', 'reset');
    }

    public static function execute($type = null, $arguments = array())
    {
        $context = array();
        $context['options'] = get_fields('option');

        $productCategoryRepository = new ProductCategoryRepository();
        $productCategories = $productCategoryRepository->findBy(array(), $context);
        $returnCategories = array();
        foreach ($productCategories as $category) {
            $category->setAkeneoCategories(array());
            foreach ($category->getAkeneoCategories() as $akeneoCategoryK => $akeneoCategoryV) {
                $returnCategories[$akeneoCategoryK] = $category->getPost()->ID;
            }
        }

        $productRepository = new ProductRepository();
        $products = $productRepository->findBy(array(), $context);
        $returnProduct = array();
        $createProduct = array();
        foreach ($products as $product) {
            $externalId = $product->getPost()->external_id;
            if (!empty($externalId)) {
                if (!isset($returnProduct[$externalId])) {
                    $returnProduct[$externalId] = $product->getPost()->ID;
                } else {
                    $idDelete = $product->getPost()->ID;
                    wp_delete_post($idDelete);
                    self::error('Good delete product post : '.$idDelete.' (because existing => '.$product->getPost()->title.')');
                }
            } else {
                $idDelete = $product->getPost()->ID;
                wp_delete_post($idDelete);
                self::error('Good delete product post : '.$idDelete. ' (because external id empty)');
            }
        }
        self::success('Find WP Existing products : '.count($returnProduct));

        $apiAkeneoProductRepository = new ApiAkeneoProductRepository();
        $apiAkeneoProductRepository->setByPage(100);
        $globalProducts = array();
        $apiApiAkeneoProducts = $apiAkeneoProductRepository->findBy(array(), $context);
        $totalP = 0;
        if (!is_wp_error($apiApiAkeneoProducts) && isset($apiApiAkeneoProducts['hydra:member'])) {
            $globalProducts = array_merge($globalProducts, $apiApiAkeneoProducts['hydra:member']);
            $totalP+= count($apiApiAkeneoProducts['hydra:member']);
            if ($apiAkeneoProductRepository->getTotalPages() > 1) {
                $totalPages = $apiAkeneoProductRepository->getTotalPages();
                for ($i = 1; $totalPages > $i; $i++) {
                    $nextPage = $i + 1;
                    $apiAkeneoProductRepository = new ApiAkeneoProductRepository();
                    $apiAkeneoProductRepository->setByPage(100);
                    $apiAkeneoProductRepository->setCurrentPage($nextPage);
                    $apiApiAkeneoProducts = $apiAkeneoProductRepository->findBy(array(), $context);
                    if (!is_wp_error($apiApiAkeneoProducts) && isset($apiApiAkeneoProducts['hydra:member'])) {
                        $globalProducts = array_merge($globalProducts, $apiApiAkeneoProducts['hydra:member']);
                        $totalP+= count($apiApiAkeneoProducts['hydra:member']);
                    }
                }
            }
        }
        self::success('Find Akeneo Pim Existing products : '.count($globalProducts));
        foreach ($globalProducts as $product) {
            $producPost = false;
            $productTitle = $product['values']['title_club'];
            $productExternalId = $product['@id'];
            if (!isset($returnProduct[$product['@id']])) {
                if (!isset($createProduct[$productExternalId])) {
                    $postData = [
                        'post_author' => 1,
                        'post_title'  => $productTitle,
                        'post_name' => sanitize_title($productTitle),
                        'post_type'   => 'product',
                        'post_status' => 'publish'
                    ];
                    $productPost = wp_insert_post($postData);
                    $createProduct[$productExternalId] = $productPost;
                    //$productPost = false;
                    if (!$productPost) {
                        self::error('Error trying insert product post for : '.$productTitle);
                    }
                    else {
                        self::success('Create product post : '.$productTitle);
                    }
                } else {
                    self::error('Existing External ID cant create product post : '.$productExternalId.' => '.$productTitle);
                }
            } else {
                $productPost = get_post($returnProduct[$product['@id']]);
                unset($returnProduct[$product['@id']]);
            }
            if ($productPost) {
                $find = $productPost;
                $wpCategories = array();
                foreach ($product['categories'] as $category) {
                    if (isset($returnCategories[$category])) {
                        $wpCategories[] = $returnCategories[$category];
                    }
                }
                if (count($wpCategories) > 0) {
                    update_field('categories', $wpCategories, $find);
                }
                update_field('external_id', $productExternalId, $find);
                update_field('external_type', 'akeneopim', $find);
                update_field('price', $product['values']['price_point'], $find);
                update_field('image_url', $product['values']['image_main'], $find);
                self::success('Update product post : '.$productTitle);
                //update_field('synch_parent_slider', 1, $find);
            }
        }
        self::error('Find WP Existing products not in Pim Akeneo : '.count($returnProduct).' we need to delete them');
        foreach ($returnProduct as $deletedProduct) {
            $idDelete = $deletedProduct;
            wp_delete_post($idDelete);
            self::error('Good delete product post : '.$idDelete. ' (because not anymore in Pim)');
        }
    }
}
