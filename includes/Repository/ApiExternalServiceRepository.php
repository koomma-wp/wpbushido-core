<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiExternalServiceRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiExternalService');
        parent::__construct();
    }
}
