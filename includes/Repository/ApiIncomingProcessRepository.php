<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiIncomingProcessRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiIncomingProcess');
        parent::__construct();
    }
}
