<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ProductCategoryRepository extends PostRepository
{
    public function __construct()
    {
        $this->setPostType('productcategory');
        $this->setEntity('ProductCategory');
        parent::__construct();
    }
}
