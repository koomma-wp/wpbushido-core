<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

use WPBushidoCore\Log\Log;

class ApiRepository extends Repository
{

    public const DEFAULT_BY_PAGE = 30;

    public function __construct()
    {
        parent::__construct();
        $this->setByPage(self::DEFAULT_BY_PAGE);
    }

    public function findBy($args, $options = false, $justId = false, $cache = array())
    {
        $args['order'] = $this->getOrderBy();
        $args['page'] = $this->getCurrentPage();
        $args['itemsPerPage'] = $this->getByPage();

        // GetEntity Construct
        $entityClass = $this->getProjectEntityClassPath();
        $entityCoreClass = $this->getCoreEntityClassPath();
        $entityObject = false;
        if (class_exists($entityClass)) {
            $entityObject = new $entityClass($options);

        } else {
            if (class_exists($entityCoreClass)) {
                $entityObject = new $entityCoreClass($options);
            }
        }
        if ($entityObject) {
            $search = $entityObject->search($args, $cache);
            if (!is_wp_error($search)) {
                $this->setData($search);
            } else {
                Log::setLog($search);
            }
            return $this->getData();
        } else {
            return new \WP_Error( 'broke', __($this->getEntity().' is not available') );
        }
    }

    public function getProjectEntityClassPath()
    {
        $entityClass = '\WPBushidoProject\ApiEntity\\';
        if (!empty($this->getEntityParent())) {
            $entityClass.= $this->getEntityParent().'\\';
        }
        $entityClass.= $this->getEntity();
        return $entityClass;
    }

    public function getCoreEntityClassPath()
    {
        $entityClass = '\WPBushidoCore\ApiEntity\\';
        if (!empty($this->getEntityParent())) {
            $entityClass.= $this->getEntityParent().'\\';
        }
        $entityClass.= $this->getEntity();
        return $entityClass;
    }

    public function setData($data)
    {
        if (!is_wp_error($data)) {
            parent::setData($data);
            if (isset($data['hydra:totalItems'])) {
                $this->setTotalItems($data['hydra:totalItems']);
            }
        }
        return $this;
    }

}
