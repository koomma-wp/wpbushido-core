<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class Repository
{

    private $byPage = -1;

    private $currentPage = 1;

    private $orderBy = '';

    private $orderWay = '';

    private $totalItems = 0;

    private $totalPages = 1;

    private $entity;

    private $entityParent = '';

    private $pagination = array();

    private $data = array();

    public function __construct()
    {
    }

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(?string $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getEntityParent(): ?string
    {
        return $this->entityParent;
    }

    public function setEntityParent(?string $entityParent): self
    {
        $this->entityParent = $entityParent;

        return $this;
    }

    public function findBy($args, $options = false)
    {
        $array = array();
        return $array;
    }

    public function getByPage()
    {
        return $this->byPage;
    }

    public function setByPage($byPage)
    {
        $this->byPage = $byPage;
        return $this;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function setCurrentPage($currentPage)
    {
        $this->currentPage = $currentPage;
        return $this;
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    public function getOrderWay()
    {
        return $this->orderWay;
    }

    public function setOrderWay($orderWay)
    {
        $this->orderWay = $orderWay;
        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    public function getTotalItems()
    {
        return $this->totalItems;
    }

    public function setTotalItems($totalItems)
    {
        $this->totalItems = $totalItems;
        if ($this->totalItems > 0) {
            if ($this->getByPage() != -1) {
                $this->setTotalPages(intval(ceil($this->totalItems / $this->getByPage())));
                $this->setPagination();
            }
        }
        return $this;
    }

    public function getTotalPages()
    {
        return $this->totalPages;
    }

    public function setTotalPages($totalPages)
    {
        $this->totalPages = $totalPages;
        return $this;
    }

    public function setPagination()
    {
        $arrayPage = array('current' => $this->getCurrentPage(), 'next' => false, 'previous' => false, 'first' => 1, 'last' => $this->getTotalPages(), 'pages' => array(), 'pages_interval' => array());
        for ($i = 1; $i <= $this->getTotalPages(); $i++) {
            if ($i > 1 && $i < $this->getTotalPages()) {
                $arrayPage['pages_interval'][] = $i;
            }
            $arrayPage['pages'][] = $i;
        }
        if (($this->getCurrentPage() + 1) <= $this->getTotalPages()) {
            $arrayPage['next'] = $this->getCurrentPage() + 1;
        }
        if (($this->getCurrentPage() - 1) >= 1) {
            $arrayPage['previous'] = $this->getCurrentPage() - 1;
        }
        $this->pagination = $arrayPage;
        return $this;
    }

    public function getPagination()
    {
        return $this->pagination;
    }


}
