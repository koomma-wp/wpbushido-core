<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ProductRepository extends PostRepository
{
    public function __construct()
    {
        $this->setPostType('product');
        $this->setEntity('Product');
        parent::__construct();
    }

    public function findBy($args, $options = false, $justId = false)
    {
        if (isset($args['range'])) {
            $range = $args['range'];
            unset($args['range']);
            $args['meta_query']	= array(
                'relation'		=> 'AND',
                array(
                    'key'	 	=> 'price',
                    'value'	  	=> $range['min'],
                    'compare' 	=> '>=',
                    'type'      => 'NUMERIC'
                ),
                array(
                    'key'	  	=> 'price',
                    'value'	  	=> $range['max'],
                    'compare' 	=> '<=',
                    'type'      => 'NUMERIC'
                ),
            );
        }
        if (isset($args['search'])) {
            $search = $args['search'];
            unset($args['search']);
            $args['s'] = $search;
        }
        if (isset($args['categories'])) {
            $categories = $args['categories'];
            if (!isset($args['meta_query'])) {
                $args['meta_query']	= array(
                    'relation'		=> 'AND'
                );
            }
            foreach ($categories as $category) {
                if (!empty($category)) {
                    $args['meta_query'][] = array(
                        'key' => 'categories',
                        'value' => '"' . strval($category) . '"',
                        'compare' => 'LIKE',
                    );
                }
            }
            unset($args['categories']);
        }
        return parent::findBy($args, $options);
    }
}
