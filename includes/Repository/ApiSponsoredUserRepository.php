<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiSponsoredUserRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiSponsoredUser');
        parent::__construct();
    }
}
