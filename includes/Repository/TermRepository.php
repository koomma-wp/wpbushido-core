<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

use WPBushidoCore\Entity\TermEntity;

class TermRepository extends Repository
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findBy($args, $options = false)
    {
        $array = parent::findBy($args);
        if (!isset($args['taxonomy'])) {
            $args['taxonomy'] = 'post_tag';
        }
        if (!isset($args['hide_empty'])) {
            $args['hide_empty'] = false;
        }
        $terms = get_terms($args);
        foreach ($terms as $term) {
            if ($term instanceof \WP_Term) {
                $termEntity = new TermEntity($term->term_id, $args['taxonomy']);
                $array[] = $termEntity->getTerm();
            }
        }
        return $array;
    }
}
