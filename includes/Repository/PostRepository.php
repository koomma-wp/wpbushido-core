<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

use WPBushidoCore\Entity\PostEntity;

class PostRepository extends Repository
{
    private $postType = 'post';

    private $wpQuery = false;

    public function __construct()
    {
        parent::__construct();
    }

    public function setPostType($postType)
    {
        $this->postType = $postType;
        return $this;
    }

    public function getPostType()
    {
        return $this->postType;
    }

    public function setWpQuery($wpQuery)
    {
        $this->wpQuery = $wpQuery;
        return $this;
    }

    public function getWpQuery()
    {
        return $this->wpQuery;
    }

    public function findBy($args, $options = false, $justId = false)
    {
        $array = parent::findBy($args, $options);
        $args = array_merge(array(
            'post_type' => $this->getPostType(),
            'posts_per_page' => $this->getByPage(),
            'orderby' => $this->getOrderBy(),
            'order' => $this->getOrderWay(),
            'paged' => $this->getCurrentPage()
        ), $args);
        $query = new \WP_Query($args);
        $this->setWpQuery($query);
        $posts = $this->getWpQuery()->posts;
        foreach ($posts as $post) {
            if ($post instanceof \WP_Post) {
                if ($justId) {
                    $array[] = $post->ID;
                } else {
                    $entityPost = '\\WPBushidoCore\Entity\\'.$this->getEntity();
                    if (class_exists($entityPost)) {
                        if (isset($options['wordpress_repeater']) && $options['wordpress_repeater'] == true) {
                            $entityPost = strtolower($this->getEntity());
                            $array[] = array($entityPost => $post);
                        } else {
                            $array[] = new $entityPost($post->ID);
                        }

                    }
                }
            }
        }
        $this->setTotalItems($query->found_posts);
        $this->setData($array);
        return $array;
    }
}
