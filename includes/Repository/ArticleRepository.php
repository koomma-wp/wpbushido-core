<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

use WPBushidoCore\Entity\Article;

class ArticleRepository extends PostRepository
{
    public function __construct()
    {
        $this->setPostType('post');
        $this->setEntity('Article');
        parent::__construct();
    }
}
