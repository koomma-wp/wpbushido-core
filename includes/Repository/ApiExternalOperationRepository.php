<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiExternalOperationRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiExternalOperation');
        parent::__construct();
    }
}
