<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiProcessRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiProcess');
        parent::__construct();
    }
}
