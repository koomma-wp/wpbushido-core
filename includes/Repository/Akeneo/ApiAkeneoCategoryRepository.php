<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository\Akeneo;

use WPBushidoCore\Repository\ApiRepository;

class ApiAkeneoCategoryRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiAkeneoCategory');
        $this->setEntityParent('Akeneo');
        parent::__construct();
    }

    public function findBy($args, $options = false, $justId = false, $cache = array())
    {
        $cache = [
            "ttl" => 3600,
            "prefix" => 'cachewpclient_api_akeneocategories'
        ];
        return parent::findBy($args, $options, $justId, $cache);
    }
}
