<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiCreditRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiCredit');
        parent::__construct();
    }
}
