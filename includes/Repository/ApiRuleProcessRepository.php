<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Repository;

class ApiRuleProcessRepository extends ApiRepository
{
    public function __construct()
    {
        $this->setEntity('ApiRuleProcess');
        parent::__construct();
    }
}
