<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Router;

class Router
{
    /**
     * Register Hooks of Router. To execute in "init" action hook
     *
     * @return void
     */
    public static function registerHooks()
    {
        // Parse Request
        add_action('parse_request', array(static::class, 'filterRequest'));
    }

    /**
     * Modify Query
     *
     * @param object $query
     * @return object
     */
    public static function filterRequest($query)
    {
        if (!is_admin()) {
            if (null === $query->matched_query) {
                $reversedQuery = array_reverse(explode('/', $query->request));
                $pagename = reset($reversedQuery);
                $findPage = get_page_by_path($pagename);
                if (!isset($findPage->ID) && isset($reversedQuery[1])) {
                    $findPage = get_page_by_path($reversedQuery[1].'/'.$pagename);
                    if (isset($findPage->ID)) {
                        $pagename = $reversedQuery[1].'/'.$pagename;
                    }
                }
                $query->query_vars['pagename'] = $pagename;
                unset($query->query_vars['error']);
            }
        }
        return $query;
    }
}