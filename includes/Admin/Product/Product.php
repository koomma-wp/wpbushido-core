<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Admin\Product;

use WPBushidoCore\Admin\Admin;

class Product extends Admin
{
    public static function registerHooks()
    {
        self::registerProduct();
    }


    public static function registerProduct()
    {
        $type = 'product';
        $labels = self::xcompilePostTypeLabels(__('Produits', WPTM_LANGUAGE_DOMAIN), __('Produits', WPTM_LANGUAGE_DOMAIN));
        $arguments = [
            'public' => true,
            'description' => __('Liste des produits.', WPTM_LANGUAGE_DOMAIN),
            'menu_icon' => 'dashicons-buddicons-community',
            'labels' => $labels,
            'publicly_queryable' => true,
            'rewrite' => array('slug' => 'product'),
            'map_meta_cap' => true
            // 'show_in_menu'  =>	'edit.php?post_type=brand',
        ];
        register_post_type($type, $arguments);
    }

}