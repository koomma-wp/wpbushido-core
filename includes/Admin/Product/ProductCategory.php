<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Admin\Product;

use WPBushidoCore\Admin\Admin;
use WPBushidoCore\Repository\Akeneo\ApiAkeneoCategoryRepository;

class ProductCategory extends Admin
{
    public static function registerHooks()
    {
        self::registerProductCategories();
        if (getenv('AKENEO_URL') !== false) {
            // key
            add_filter('acf/load_field/key=field_5ce66ed49b6d3', array('WPBushidoCore\Admin\Product\ProductCategory', 'getApiCategories'));
        }
    }

    public static function registerProductCategories()
    {
        $type = 'productcategory';
        $labels = self::xcompilePostTypeLabels(__('Catégories de produit', WPTM_LANGUAGE_DOMAIN), __('Catégories de produit', WPTM_LANGUAGE_DOMAIN));
        $arguments = [
            'public' => true,
            'description' => __('Liste des catégories de produit.', WPTM_LANGUAGE_DOMAIN),
            'menu_icon' => 'dashicons-performance',
            'labels' => $labels,
            'publicly_queryable' => true,
            'rewrite' => array('slug' => 'productcategory'),
            'map_meta_cap' => true
            // 'show_in_menu'  =>	'edit.php?post_type=brand',
        ];
        register_post_type($type, $arguments);
    }

    public static function getApiCategories($field)
    {
        // reset choices
        $field['choices'] = array();

        // get the textarea value from options page without any formatting
        $apiAkeneoCategoryRepository = new ApiAkeneoCategoryRepository();
        $apiAkeneoCategories = $apiAkeneoCategoryRepository->findBy(array(), self::getContext());
        $choices = array();
        if (!is_wp_error($apiAkeneoCategories)) {
            if (isset($apiAkeneoCategories['hydra:member'])) {
                foreach ($apiAkeneoCategories['hydra:member'] as $category) {
                    if ($category['parent'] == 'master') {
                        $choices[$category['@id']] = $category['label'];
                    }
                }
            }
        }
        // remove any unwanted white space
        $choices = array_map('trim', $choices);

        // loop through array and add to field 'choices'
        if (is_array($choices)) {
            foreach ($choices as $choiceK => $choiceV) {
                $field['choices'][$choiceK] = $choiceV;
            }
        }
        // return the field
        return $field;
    }
}