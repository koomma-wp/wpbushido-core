<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Admin\IncomingProcess;

use Timber\Timber;
use WPBushido\Helpers\Data as WPBushidoData;
use WPBushidoCore\Admin\Admin;
use WPBushidoCore\ApiEntity\ApiIncomingProcess;
use WPBushidoCore\Page\Page;
use WPBushidoCore\Repository\ApiProcessRepository;

class IncomingProcess extends Admin
{

    const FIELD_FILE = 'file';

    public static function registerHooks()
    {
        add_action('admin_menu', array(self::class, 'registerMenu'));
        wp_enqueue_style('acf-pro-input');
        wp_enqueue_style('acf-pro-field-group');
    }


    public static function registerMenu()
    {
        // process listing
        add_menu_page(
            __('Processus'),
            __('Processus'),
            'publish_posts',
            'list-incoming-process',
            array(self::class, 'displayAdminList'),
            'dashicons-sos',
            30
        );

        // process edit
        add_submenu_page(
            'options.php',
            __('Créer un processus'),
            __('Créer un processus'),
            'publish_posts',
            'new-incoming-process',
            array(self::class, 'displayAdminCreate')
        );

        // process detail
        add_submenu_page(
            'options.php',
            __('Voir un processus'),
            __('Voir un processus'),
            'publish_posts',
            'view-incoming-process',
            array(self::class, 'displayAdminView')
        );
    }

    public static function displayAdminList()
    {
        $timber = new Timber();
        $context = $timber::get_context();
        if (isset($_POST['formAction']) && $_POST['formAction'] == 'saveProcess') {
            $process = self::saveProcess($context);
            if (isset($process['@id'])) {
                $context['success'] = __('New processus added');
            }
            else {
                if (isset($process['error'])) {
                    $context['error'] = __('Error during save : ');
                    foreach ($process['error'] as $error) {
                        $context['error'].= '<br/>'.$error;
                    }
                } else {
                    $context['error'] = __('Error during save');
                }
            }
        }
        $context['use_tpl'] = 'process/list.html.twig';
        ob_start();
        $wpListTable = new IncomingProcessList();
        $wpListTable->prepare_items();
        $wpListTable->display();
        $context['admin_list'] = ob_get_clean();
        parent::displayAdmin($context);
    }

    public static function displayAdminCreate()
    {
        $timber = new Timber();
        $context = $timber::get_context();
        $context['use_tpl'] = 'process/create.html.twig';
        $apiIncomingProcessRepository = new ApiProcessRepository();
        $processes = $apiIncomingProcessRepository->findBy(array());
        $data = array('data' => array(), 'totalitems' => 0);
        if (!is_wp_error($processes) && isset($processes['hydra:member']) && isset($processes['hydra:totalItems'])) {
            $data['data'] = $processes['hydra:member'];
            $data['totalitems'] = $processes['hydra:totalItems'];
        }
        $context['processes'] = $data;
        ob_start();
        wp_nonce_field( 'process-save', 'process-save-form' );
        submit_button(__('Add'));
        $context['submit_button'] = ob_get_clean();
        parent::displayAdmin($context, true);
    }

    public static function displayAdminView()
    {
        $timber = new Timber();
        $context = $timber::get_context();
        $context['use_tpl'] = 'process/view.html.twig';
        $page = new Page($context);
        $incomingParams = WPBushidoData::checkIncomingAjax();
        if (isset($incomingParams['id'])) {
            $id = $incomingParams['id'];
            $incomingProcess = new ApiIncomingProcess($context, $id);
            $getProcess = $incomingProcess->get(array());
            if (!is_wp_error($getProcess) && isset($getProcess['@id'])) {
                $context['incoming_process'] = $getProcess;
            } else {
            }
        }
        ob_start();
        parent::displayAdmin($context, true);
    }


    public static function saveProcess($context)
    {
        $data = array();
        $return = false;

        $page = new Page($context);
        $incomingParams = WPBushidoData::checkIncomingAjax();
        if (isset($incomingParams['process']) && !empty($incomingParams['process'])) {

            $data['process'] = $incomingParams['process'];

            if (isset($_FILES[self::FIELD_FILE]['name']) && $_FILES[self::FIELD_FILE]['type'] == 'text/csv') {
                $dataFile = file_get_contents($_FILES[self::FIELD_FILE]['tmp_name']);
                if (!empty($dataFile)) {
                    $data['file'] = 'data:text/csv;base64,' . base64_encode($dataFile);
                }
            }

            $data['arguments'] = array();
            if (isset($incomingParams['arguments']) && !empty($incomingParams['arguments'])) {
                $arguments = explode(';', $incomingParams['arguments']);
                foreach ($arguments as $argument) {
                    $args = explode(':', $argument);
                    if (count($args) == 2) {
                        $argsData = $args[1];
                        if (is_numeric($argsData)) {
                            $argsData = intval($argsData);
                        }
                        $data['arguments'][$args[0]] = $argsData;
                    }
                }
            }

            $incomingProcess = new ApiIncomingProcess($context);
            $postIncomingProcess = $incomingProcess->post($data);
            if (!is_wp_error($postIncomingProcess) && isset($postIncomingProcess['@id'])) {
                $return = $postIncomingProcess;
            } else {
                foreach ($postIncomingProcess->get_error_messages() as $error) {
                    $errorApi = json_decode($error->getBody()->getContents());
                    if (isset($errorApi->violations)) {
                        $return['error'] = array();
                        foreach ($errorApi->violations as $violation) {
                            $return['error'][] = $violation->message;
                        }
                    }
                }
            }
        }
        return $return;
    }

}