<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Admin\IncomingProcess;

use WPBushidoCore\Admin\AdminList;
use WPBushidoCore\Repository\ApiIncomingProcessRepository;

class IncomingProcessList extends AdminList
{
    public $context; // Timber Context

    public function __construct()
    {
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'process',     //singular name of the listed records
            'plural'    => 'processes',    //plural name of the listed records
            'ajax'      => true        //does this table support ajax?
        ) );
    }

    public function column_default($item, $column_name)
    {
        $data = '';
        $style = '';
        switch ($column_name) {
            case 'id':
                $data = $item['@id'];
                $data = str_replace('/api/incoming_processes/', '', $data);
                break;

            case 'process':
                $data = $item[$column_name]['name'];
                break;

            case 'status':
                $data = $item[$column_name];
                if ($data == 'PROCESSED') {
                    $style.= 'color:green;';
                } else if ($data == 'UNPROCESSED') {
                    $style.= 'color:blue;';
                }
                else if ($data == 'PROCESSED_WITH_ERROR') {
                    $style.= 'color:orange;';
                }
                else {
                    $style.= 'color:red;';
                }
                break;

            case 'createdAt':
            case 'executedAt':
                $data = $item[$column_name];
                if (!empty($data)) {
                    $data = date_i18n(get_option('date_format') . ' -   ' . get_option('time_format'), strtotime($data));
                }
                break;
            default:
                $data = $item[$column_name]; //Show the whole array for troubleshooting purposes
        }
        echo '<span style="'.$style.'">'.$data.'</span>';
    }

    public function column_id($item)
    {
        //Build row actions
        $data = $item['@id'];
        $data = str_replace('/api/incoming_processes/', '', $data);
        $actions = array(
            'edit'      => sprintf('<a href="?page=view-incoming-process&action=%s&id=%s">Details</a>','detail',$data),
            // 'delete'    => sprintf('<a href="?page=%s&action=%s&dealer=%s">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
        );

        //Return the title contents
        return sprintf('%1$s <span style="color:silver">(%2$s)</span>%3$s',
            /*$1%s*/ $data,
            /*$2%s*/ $item['@id'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }


    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("car")
            /*$2%s*/ $item['id']                //The value of the checkbox should be the record's id
        );
    }

    public function get_columns()
    {
        $columns = array(
            // 'cb'        => '<input type="checkbox" />', //Render a checkbox instead of text
            'id'       =>  __('API Id'),
            'createdAt'    => __('Créer le'),
            'executedAt'     => __('Exécuter le'),
            'status'     => __('Etat'),
            'process'  => __('Type'),
        );
        return $columns;
    }


    public function get_sortable_columns()
    {
        $sortable_columns = array(

        );
        return $sortable_columns;
    }


    public function get_bulk_actions()
    {
        $actions = array(
            // 'delete'    => 'Delete'
        );
        return $actions;
    }

    function process_bulk_action()
    {

        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }

    }

    public function prepare_items()
    {
        $per_page = 30;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $current_page = $this->get_pagenum();
        $processes = $this->getProcesses($per_page, $current_page);
        if (isset($processes['data'])) {
            $data = $processes['data'];
            $total_items = $processes['totalitems'];
        }
        else {
            $data = array();
            $total_items = 0;
        }

        $this->items = $data;

        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );
    }

    /**
     * Display the rows of records in the table
     * @return string, echo the markup of the rows
     */
    public function display_rows() {

        //Get the records registered in the prepare_items method
        $records = $this->items;

        //Get the columns registered in the get_columns and get_sortable_columns methods
        list($columns, $hidden) = $this->get_column_info();

        //Loop for each record
        if (!empty($records)) {
            foreach ($records as $rec) {
                $this->single_row($rec);
            }
        }
    }


    private function getProcesses($per_page, $current_page)
    {
        $data = array('data' => array(), 'totalitems' => 0, 'totalpages' => 1);
        $context = \Timber\Timber::get_context();
        $args = [
            'itemsPerPage' => $per_page,
            'page' => $current_page
        ];
        $incomingProcessRepository = new ApiIncomingProcessRepository();
        $incomingProcessRepository->setByPage($per_page);
        $incomingProcessRepository->setCurrentPage($current_page);
        $incomingProcesses = $incomingProcessRepository->findBy(array());
        if (!is_wp_error($incomingProcesses) && isset($incomingProcesses['hydra:member']) && isset($incomingProcesses['hydra:totalItems'])) {
            $data['data'] = $incomingProcesses['hydra:member'];
            $data['totalitems'] = $incomingProcesses['hydra:totalItems'];
        }
        return $data;
    }
}