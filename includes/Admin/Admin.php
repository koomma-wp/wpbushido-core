<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Admin;

use Timber\Timber;
use WPBushido\Admin\AdminPage;

class Admin extends AdminPage
{
    public static function getContext()
    {
        return Timber::get_context();
    }

    public static function registerHooks()
    {
        $adminPath = BUSHIDO_WP_CORE_PLUGIN_PATH. 'includes/Admin';
        if ($handle = opendir($adminPath)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (is_dir($adminPath.'/'.$entry)) {
                        if ($handlesub = opendir($adminPath.'/'.$entry)) {
                            while (false !== ($subentry = readdir($handlesub))) {
                                if ($subentry != "." && $subentry != "..") {
                                    $namePageClass = 'WPBushidoCore\\Admin\\' .$entry.'\\'. str_replace(array('-', '_', '.php'), array('', '', ''), $subentry);
                                    if (class_exists($namePageClass)) {
                                        if (method_exists($namePageClass, 'registerHooks')) {
                                            $namePageClass::registerHooks();
                                        }
                                    }
                                }
                            }
                            closedir($handlesub);
                        }
                    }
                }
            }
            closedir($handle);
        }
    }

    public static function xcompilePostTypeLabels($singular = 'Post', $plural = 'Posts')
    {
        $p_lower = strtolower($plural);
        $s_lower = strtolower($singular);

        return [
            'name' => $plural,
            'singular_name' => $singular,
            'add_new_item' => "Nouveau $singular",
            'edit_item' => "Modifier $singular",
            'view_item' => "Voir $singular",
            'view_items' => "Voir $plural",
            'search_items' => "Rechercher $plural",
            'not_found' => "Aucun $p_lower trouvé",
            'not_found_in_trash' => "Aucun $p_lower trouvé dans la corbeille",
            'parent_item_colon' => "Parent $singular",
            'all_items' => "Tous les $plural",
            'archives' => "$singular en archive",
            'attributes' => "$singular attributs",
            'insert_into_item' => "Insérer dans $s_lower",
            'uploaded_to_this_item' => "Charger un media dans $s_lower",
        ];
    }

    public static function displayAdmin($incomingContext, $timberContext = false, $plugin_path = false)
    {
        parent::displayAdmin($incomingContext, $timberContext, plugin_dir_path( __FILE__ ));
    }
}