<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Page;

use WPBushidoCore\ApiEntity\Akeneo\ApiAkeneoProduct;
use WPBushidoCore\Repository\Akeneo\ApiAkeneoProductRepository;
use WPBushidoCore\Repository\ProductRepository;

class PageCatalog extends Page
{
    public function __construct(array $context)
    {
        parent::__construct($context);
        $this->setMainTpl('catalog/product-list.html.twig');
        $this->setInPageRouterArgs('product_slug', 'product_slug');
    }

    public function process()
    {
        parent::process();
        $this->getProductCategories();
        $this->findProduct();
        $this->renderTpl();
    }

    public function findProduct()
    {
        $context = $this->getContext();
        $productRepository = new ProductRepository();
        $productRepository->setByPage(30);

        // Reset
        $resetFilters = false;
        $searchFilters = array();
        if (isset($context['incoming_args']['categories']) || isset($context['incoming_args']['search']) || isset($context['incoming_args']['range'])) {
            $resetFilters = true;
        }
        // Incoming Attributes
        $filters = $this->incomingParamsPersistence(
            array('pagination', 'search', 'categories', 'range', 'order'),
            $resetFilters
        );

        // Pagination
        if (isset($filters['order'])) {
            $arrayOrder = explode(',', $filters['order']);
            $searchFilters['order'] = $filters['order'];
            if (isset($arrayOrder[1])) {
                $productRepository->setOrderWay(strtoupper($arrayOrder[1]));
            }
            if (!empty($arrayOrder[0])) {
                $filters['meta_key'] = $arrayOrder[0];
                $productRepository->setOrderBy('meta_value_num');
            }
        } else {
            $filters['meta_key'] = 'price';
            $searchFilters['order'] = 'price,asc';
            $productRepository->setOrderBy('meta_value_num');
            $productRepository->setOrderWay('ASC');
        }

        // Order
        if (isset($filters['pagination'])) {
            $productRepository->setCurrentPage($filters['pagination']);
        }

        // Incoming Params
        if (isset($filters['search'])) {
            $searchFilters['search'] = trim(strip_tags($filters['search']));
            $searchFilters['search'] = explode(' ', $searchFilters['search']);
        }
        if (isset($filters['categories'])) {
            $searchFilters['categories'] = $filters['categories'];
        }
        if (isset($filters['range'])) {
            $ranges = explode(',', $filters['range']);
            $searchFilters['range'] = array();
            if (count($ranges) == 2) {
                $searchFilters['range']['min'] = $ranges[0];
                $searchFilters['range']['max'] = $ranges[1];
                $filters['range'] = array();
                $filters['range']['min'] = $ranges[0];
                $filters['range']['max'] = $ranges[1];
            }
        } else {
            $filters['range'] = array();
            $filters['range']['min'] = 1;
            $filters['range']['max'] = 50000;
        }

        $this->setInContext('filters', $filters);
        $this->setInContext('searchFilters', $searchFilters);

        if (isset($context['page_vars']['product_slug']) && !empty($context['page_vars']['product_slug'])) {
            $apiApiAkeneoProduct = new ApiAkeneoProduct($context, $context['page_vars']['product_slug']);
            $product = $apiApiAkeneoProduct->get(array());
            if (!is_wp_error($product)) {
                $this->setInContext('product', $product);
                $this->setMainTpl('catalog/product-sheet.html.twig');
            } else {
                $this->setMainTpl('404.html.twig');
            }
        } else {
            $products = $productRepository->findBy($filters, $context);
            $this->setInContext('products', $productRepository);
        }
    }
}