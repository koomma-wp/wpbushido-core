<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Page;

use WPBushido\Page\Page as WPBushidoPage;
use WPBushido\Helpers\Data as WPBushidoData;
use WPBushido\Client\Client as WPBushidoClient;
use WPBushidoCore\Cache\Cache;
use WPBushidoCore\Menu\Menu as WPCoreMenu;
use WPBushidoCore\Repository\Akeneo\ApiAkeneoCategoryRepository;
use WPBushidoCore\Repository\ProductCategoryRepository;
use WPBushidoCore\Seo\Seo as WPCoreSeo;
use WPBushidoProject\Entity\User;

class Page
{
    private $context = array();
    private $pageTpl = 'home.html.twig';
    private $pageRouterArgs = array();
    private $cacheManager = false;

    public function __construct(array $context)
    {
        $this->setContext($context);
    }

    public function process()
    {
        // Incoming Parameters
        $this->setInContext('incoming_args', WPBushidoData::checkIncomingAjax());
        // Menu / Pages
        $menu = new WPCoreMenu($this->getContext());
        $this->setInContext('menu', $menu->getMenu());
        // Seo
        $seo = new WPCoreSeo($this->getContext());
        $this->setInContext('page_seo', $seo->getSeo());
        // Page Vars / Url
        $this->setInContext('page_vars', $this->getPageVars());
        // Sub Pages
        $this->setInContext('page_subpages', $this->getSubPages());
        // Cache Manager
        $keyConf = 'apisymfony';
        if (!defined('API_CONFIG') || !array_key_exists($keyConf, API_CONFIG)) {
        } else {
            $config = API_CONFIG[$keyConf][$this->getContext()['options']['country']];
            if (isset($config['cache'])) {
                $this->setCacheManager(Cache::instance($config['cache']));
            }
        }
        $this->checkUser();
    }

    /**
     * Add Page Vars for Subpage Rewrite Rules
     *
     * @param array $vars : vars arrays
     * @return array $vars
     */
    public function addPageVars($vars)
    {
        foreach ($this->pageRouterArgs as $arg) {
            $vars[] = $arg;
        }
        return $vars;
    }

    /**
     * Get defined pages url rewrite vars
     *
     * @return array $returnVars
     */
    public function getPageVars()
    {
        $returnVars = array();
        foreach ($this->pageRouterArgs as $arg) {
            $returnVars[$arg] = get_query_var($arg);
        }
        return $returnVars;
    }

    /**
     * Set dynamic Rewrite rules with regex
     *
     * @return void
     */
    public function setPagesVarsRules()
    {
        $pageTplName = strtolower(str_replace(array('WPBushidoCore\\Page\\Page', 'WPBushidoProject\\Page\\Page'), array('', ''), get_class($this)));
        $pageSlug = WPBushidoClient::getPageSlugByTpl($pageTplName);
        if (!empty($pageSlug) && count($this->pageRouterArgs) > 0) {
            $baseRegex = '^([^/]*)?\/?([^/]*)?\/?([^/]*)?\/?'.$pageSlug.'/';
            $queryRegex = 'index.php?pagename='.$pageSlug;
            $baseRegexAdd = '';
            $matchCount = 4;
            foreach ($this->pageRouterArgs as $arg) {
                if (!empty($baseRegexAdd)) {
                    $baseRegexAdd.= '\/?';
                }
                $baseRegexAdd.= '([^/]*)';
                $queryRegex.= '&'.$arg.'=$matches['.$matchCount.']';
                $matchCount++;
            }
            $baseRegex = $baseRegex.$baseRegexAdd.'\/?';
            add_rewrite_rule(
                $baseRegex,
                $queryRegex,
                'top'
            );
        }
    }

    /**
     * Build page subpages data array
     *
     * @return array $subPages : with subpages, current_page, menu
     */
    public function getSubPages($forcePage = false)
    {
        $context = $this->getContext();
        $pageObj = 'page_object';
        if ($forcePage) {
            $context = $forcePage;
            $pageObj = 'page';
        }
        $subPage = false;
        $subPages = array('subpages' => array(), 'current_page' => false, 'menu' => array());
        if (isset($context['page_vars']['subpage'])) {
            $subPage = $context['page_vars']['subpage'];
        }
        if (isset($context['page_acf']['subpages']) && is_array($context['page_acf']['subpages'])) {
            foreach ($context['page_acf']['subpages'] as $keyS => $subpage) {;
                $subpage['url'] = $context[$pageObj]->link().$subpage['subpage_slug'].'/';
                $subPages['subpages'][$subpage['subpage_key']] = $subpage;
                if (!$subPages['current_page']) {
                    $subPages['current_page'] = $subpage;
                }
                if ($subPage == $subpage['subpage_slug']) {
                    $subPages['current_page'] = $subpage;
                }
                if (isset($subpage['subpage_menu_order']) && !empty($subpage['subpage_menu_order'])) {
                    $subPages['menu'][intval($subpage['subpage_menu_order'])] = $subpage;
                }
            }
            if (isset($subPages['menu'])) {
                ksort($subPages['menu']);
            }
        }
        return $subPages;
    }

    /**
     * Process Sub Pages and launch subpages
     *
     * @return void
     */
    public function processSubPage()
    {
        $context = $this->getContext();
        if (isset($context['page_vars']['subpage']) && !empty($context['page_vars']['subpage'])) {
            $nameMethod = 'processSubPage'.str_replace('-', '', strtolower($context['page_subpages']['current_page']['subpage_key']));
            $this->getSubPageSessionInContext();
            if (method_exists($this, $nameMethod)) {
                $this->$nameMethod();
            }
            $this->flushStepsSession('error');
        } else {
            wp_redirect($context['page_subpages']['current_page']['url']);
        }
    }

    /**
     * Set pageRouterArgs property
     *
     * @param array $pageRouterArgs
     * @return object
     */
    public function setPageRouterArgs($pageRouterArgs)
    {
        $this->pageRouterArgs = $pageRouterArgs;
        return $this;
    }

    /**
     * Get pageRouterArgs property
     *
     * @return array pageRouterArgs
     */
    public function getPageRouterArgs()
    {
        return $this->pageRouterArgs;
    }

    /**
     * Set pageRouterArgs property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInPageRouterArgs(string $key, $values)
    {
        $this->pageRouterArgs[$key] = $values;
    }

    /**
     * Set context (from Timber) property
     *
     * @param array $context
     * @return object
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Get context (from Timber) property
     *
     * @return array context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set context property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInContext(string $key, $values)
    {
        $this->context[$key] = $values;
    }

    /**
     * Set page twig main template
     *
     * @param string $pageTpl
     * @return object
     */
    public function setMainTpl($pageTpl)
    {
        $this->pageTpl = $pageTpl;
        return $this;
    }

    /**
     * Get page twig main template
     *
     * @return string $pageTpl
     */
    public function getMainTpl()
    {
        return $this->pageTpl;
    }

    /**
     * Set Cache Manager
     *
     * @param object $cacheManager
     * @return object
     */
    public function setCacheManager($cacheManager)
    {
        $this->cacheManager = $cacheManager;
        return $this;
    }

    /**
     * Get Cache Manager
     *
     * @return mixed $cacheManager
     */
    public function getCacheManager()
    {
        return $this->cacheManager;
    }

    /**
     * Render Twig Template
     *
     * @param string $tpl
     * @return void
     */
    public function renderTpl($tpl = '')
    {
        if (empty($tpl)) {
            $tpl = $this->getMainTpl();
        }
        if (!empty($tpl)) {
            $this->setContext(WPBushidoClient::getRenderTwig($tpl, $this->getContext()));
        }
    }

    /**
     * Set steps sessions content
     *
     * @param array $sessioncontent
     * @param mixed $sessionkey
     * @return void
     */
    public function setStepsSession($sessioncontent, $sessionkey = false)
    {
        if ($sessionkey) {
            if (!isset($_SESSION[$this->getStepsSessionName()])) {
                $_SESSION[$this->getStepsSessionName()] = array();
            }
            $_SESSION[$this->getStepsSessionName()][$sessionkey] = $sessioncontent;
        } else {
            $_SESSION[$this->getStepsSessionName()] = $sessioncontent;
        }
    }

    /**
     * Get steps sessions content
     *
     * @return array
     */
    public function getStepsSession()
    {
        if (!isset($_SESSION[$this->getStepsSessionName()])) {
            return array();
        } else {
            return $_SESSION[$this->getStepsSessionName()];
        }
    }

    /**
     * Flush / Unset step session
     *
     * @return void
     */
    public function flushStepsSession($key = false)
    {
        if ($key && isset($_SESSION[$this->getStepsSessionName()][$key])) {
            unset($_SESSION[$this->getStepsSessionName()][$key]);
        } else if (!$key && isset($_SESSION[$this->getStepsSessionName()])) {
            unset($_SESSION[$this->getStepsSessionName()]);
        }
    }

    /**
     * Get steps sessions class name
     *
     * @return string
     */
    public function getStepsSessionName()
    {
        $classname = strtolower(get_class($this));
        return 'wp_'.substr($classname, strrpos($classname, '\\') + 1);
    }

    /**
     * Set in context steps session
     *
     * @return void
     */
    public function getSubPageSessionInContext()
    {
        $this->setInContext('page_subpages_session', $this->getStepsSession());
    }

    public function incomingParamsPersistence($filters, $reset = false)
    {
        $context = $this->getContext();
        $return = array();
        foreach ($filters as $filter) {
            if (isset($context['incoming_args'][$filter])) {
                $return[$filter] = $context['incoming_args'][$filter];
                $this->setStepsSession($context['incoming_args'][$filter], $filter);
            } else if ($this->getStepsSession()[$filter]) {
                if ($reset) {
                    $this->flushStepsSession($filter);
                } else {
                    $return[$filter] = $this->getStepsSession()[$filter];
                }

            }
        }
        return $return;
    }

    public function getProductCategories()
    {
        $apiAkeneoCategoryRepository = new ApiAkeneoCategoryRepository();
        $apiAkeneoCategories = $apiAkeneoCategoryRepository->findBy(array(), self::getContext());
        $apiCategory = array();
        if (!is_wp_error($apiAkeneoCategories)) {
            if (isset($apiAkeneoCategories['hydra:member'])) {
                foreach ($apiAkeneoCategories['hydra:member'] as $category) {
                    if ($category['parent'] == 'master') {
                        $apiCategory[$category['@id']] = $category;
                    }
                }
            }
        }
        $productCategoryRepository = new ProductCategoryRepository();
        $productCategories = $productCategoryRepository->findBy(array(), self::getContext());
        $returnCategories = array();
        foreach ($productCategories as $category) {
            $category->setAkeneoCategories(array());
            $returnCategories[$category->getPost()->ID] = $category;
        }
        $this->setInContext('product_categories', $returnCategories);
    }

    public function checkUser()
    {
        $context = $this->getContext();
        if (isset($context['user'])) {
            if (isset($context['user']->ID)) {
                $user = new User($context['user']->ID);
                $user->setContext($context);
                $user->wpGetUser();
            }
            else {
                $user = new User(false);
                $user->setContext($context);
            }
        } else {
            $user = new User(false);
            $user->setContext($context);
        }
        $this->setInContext('user', $user);
    }
}