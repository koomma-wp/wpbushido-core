<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoProject
 */

namespace WPBushidoCore\Page;

use WPBushidoProject\ApiEntity\ApiMailReporting;
use WPBushidoCore\Log\Log;

class PageEventmail extends Page
{

    public function process()
    {
        $this->setMainTpl('event_mail.html.twig');
        parent::process();
        $this->mailGateway();
    }

    public function mailGateway()
    {
        $inputJSON = file_get_contents('php://input');
        $input = json_decode($inputJSON, TRUE);
        if (is_array($input) && count($input) > 0) {
            $apiMailReporting = new ApiMailReporting($this->getContext(), false);
            $mailEventReporting = $apiMailReporting->postCustom($apiMailReporting->getEntity().'/events', $input);
            if (!is_wp_error($mailEventReporting)) {
                Log::setLog('Sendgrid Hook receive with ' . count($input) . ' event(s)', 'INFO');
            } else {
                Log::setLog($mailEventReporting);
            }
        }
    }
}