<?php
/**
 * WPBushidoProject Plugin
 *
 * @package WPBushidoProject
 */

namespace WPBushidoCore\Page;

class Pagehomepage extends Page
{
    public function process()
    {
        parent::process();
        $this->renderTpl();
    }

    public function pageRouting()
    {

    }
}