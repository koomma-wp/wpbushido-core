<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Page;

use WPBushidoProject\ApiEntity\ApiMailReporting;
use WPBushidoCore\ApiEntity\ApiUser;
use WPBushidoCore\ApiEntity\ApiUserToken;
use WPBushidoProject\Entity\User;

class PagePreviewmail extends Page
{

    public function process()
    {
        $this->setMainTpl('preview_mail.html.twig');
        parent::process();
        $this->previewMail();
    }

    public function previewMail()
    {
        $context = $this->getContext();
        $goToken = false;
        $dataEmail = array();
        $user = new User(false);
        if (isset($context['incoming_args']['ref']) && !empty($context['incoming_args']['ref']) && isset($context['incoming_args']['mail']) && !empty($context['incoming_args']['mail'])) {
            $ref = (int) $context['incoming_args']['ref'];
            $mail = sha1($ref.getenv('APP_NAME'));
            if ($mail == $context['incoming_args']['mail']) {
                $this->setInContext('preview_url', $context['menu']['modules']['preview_mail']['url'] . '?ref=' . $ref . '&mail=' . $mail);
                $apiMailReporting = new ApiMailReporting($this->getContext(), $ref);
                $mailReporting = $apiMailReporting->get(array());
                if (!is_wp_error($mailReporting) && isset($mailReporting['@id']) && isset($mailReporting['template'])) {
                    $apiUser = new ApiUser($this->getContext(), $mailReporting['user']);
                    $apiUser->get(array());
                    if (!is_wp_error($apiUser) && isset($apiUser->getData()['@id']) && !empty($apiUser->getData()['@id'])) {
                        $user->setApiUser($apiUser);
                        $user->getPdvUserGroups();
                        if ($apiMailReporting->getUserToken()) {

                        }
                    }
                    $apiKey = getenv('SENDGRID_API_KEY');
                    $sg = new \SendGrid($apiKey);
                    $template_id = $mailReporting['template'];
                    $response = $sg->client->templates()->_($template_id)->get();
                    $responseJson = json_decode($response->body(), true);
                    $contentSendGrid = '';
                    foreach ($responseJson['versions'] as $version) {
                        if ($version['active'] === 1) {
                            $contentSendGrid = $version['html_content'];
                        }
                    }
                    $this->setInContext('user', $user);
                    $contentSendGrid = str_replace($apiMailReporting->getReplaceContent()['src'], array_map(array($this, 'findInContextValue') , $apiMailReporting->getReplaceContent()['dst']), $contentSendGrid);
                    echo $contentSendGrid;
                }
            }
        }
    }

    public function findInContextValue($stopWord)
    {
        $context = $this->getContext();
        return \WPBushido\Helpers\Data::findInContextValue($stopWord, $context);
    }
}