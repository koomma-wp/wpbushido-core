<?php
/**
 *
 * @author Philippe AUTIER <philippe.autier@koomma.fr>
 */

namespace WPBushidoCore\Api\ApiManager;

use \WPBushido\Api\ApiClient\ApiClient;

class ApiSymfony
{

    /**
     * @var ApiClient
     */
    protected $client;

    /**
     * @var ApiCountry
     */
    protected $country;

    /**
     * @var ApiLang
     */
    protected $lang;

    /**
     * @var ApiLocalMode
     */
    protected $local;

    /**
     * @var string
     */
    protected $baseUri = '/api';

    /**
     * Current Cardayz API Manager instance
     *
     * @var self
     */
    private static $_instance = null;

    /**
     * Front User API Client
     *
     * @var ApiClient
     */
    protected $frontUserClient;

    /**
     * Create or retrieve instance of ApiManager
     *
     * @return self
     */
    public static function instance($lang, $country, $blog)
    {
        if (self::$_instance == null) {
            self::$_instance = new self($lang, $country, $blog);
        }
        return self::$_instance;
    }

    public function __construct($lang, $country, $blog)
    {
        $keyConf = 'apisymfony';
        if (!defined('API_CONFIG') || !array_key_exists($keyConf, API_CONFIG)) {
            return false;
        }
        $this->setCountry($country);
        $this->setLang($lang);
        $config = API_CONFIG[$keyConf][$country];
        $options = [
            'username'         => $config['api_login'],
            'password'         => $config['api_password'],
            'tokenpath'        => $this->baseUri.'/auth',
            'tokenname'        => 'apisymfony-wp-client',
            'tokenmethod'      => ApiClient::TOKEN_METHOD_POST,
            'tokenbody'        => 'token',
            'tokengetauthmode' => ApiClient::TOKEN_GET_AUTHMODE_JSON,
            'tokenauthmode'    => ApiClient::TOKEN_AUTH_BEARER,
            //'tokenauthfield'   => 'apikey',
            'debug'            => (array_key_exists('debug', $config) ? $config['debug'] : false),
            //'debug' => true
        ];
        $guzzleOptions = [
            'base_uri'         => $config['api_url'],
        ];

        // Cache
        if (isset($config['cache'])) {
            $options['cache'] = $config['cache'];
        }
        $this->client = new ApiClient($options, $guzzleOptions);
    }

    public function setBaseUri($baseUri)
    {
        $this->baseUri = $baseUri;
    }

    public function getBaseUri()
    {
        return $this->baseUri;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getLang()
    {
        return $this->lang;
    }

    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    public function getLocal()
    {
        return $this->local;
    }

    public function setLocal($local)
    {
        $this->local = $local;
    }

    public function getParams($params, $route, $nocountry = true)
    {
        if (!$nocountry) {
            $params['country'] = $this->getCountry();
        }
        if ($this->getLocal()) {
            $params['f'] = '/'.$route;
        }
        return $params;
    }


    /**
     * Create Entity - POST REQUEST
     *
     * @apiclient $client
     * @param string $entity
     * @param array $params
     * @return array|WP_Error
     */
    public function createEntity($entity, $params = array())
    {
        $route = $entity;
        $params = self::getParams($params, $route);
        return $this->client->request('POST', $this->baseUri.'/'.$route, [
            'json' => $params
        ]);
    }

    /**
     * Update Entity - PUT REQUEST
     *
     * @apiclient $client
     * @param string $entity
     * @param array $params
     * @return array|WP_Error
     */
    public function updateEntity($ref, $entity, $params = array())
    {
        $route = $entity.'/';
        if (!is_int($ref)) {
            $ref = str_replace($this->baseUri.'/'.$route, '', $ref);
        }
        $route.= $ref;
        $params = self::getParams($params, $route);
        return $this->client->request('PUT', $this->baseUri.'/'.$route, [
            'json' => $params
        ]);
    }

    /**
     * Update Entity - PATCH REQUEST
     *
     * @apiclient $client
     * @param string $entity
     * @param array $params
     * @return array|WP_Error
     */
    public function updatePatchEntity($ref, $entity, $params = array())
    {
        $route = $entity.'/';
        if (!is_int($ref)) {
            $ref = str_replace($this->baseUri.'/'.$route, '', $ref);
        }
        $route.= $ref;
        $params = self::getParams($params, $route);
        return $this->client->request('PATCH', $this->baseUri.'/'.$route, [
            'json' => $params
        ]);
    }

    /**
     * Get Entity by Id (Ref)
     *
     * @apiclient $client
     * @param string $ref
     * @param string $entity
     * @param array $params
     * @param array $cache
     * @return array|WP_Error
     */
    public function getEntity($ref, $entity, $params = array(), $cache = array())
    {
        $route = $entity.'/';
        if (!is_int($ref)) {
            $ref = str_replace($this->baseUri.'/'.$route, '', $ref);
        }
        $route.= $ref;
        $params = self::getParams($params, $route);
        return $this->client->request('GET', $this->baseUri.'/'.$route, [
            "query" => $params
        ], [], $cache);
    }

    /**
     * Get Entity With Custom Route
     *
     * @apiclient $client
     * @param string $route
     * @param array $params
     * @return array|WP_Error
     */
    public function getEntityWithCustomRoute($route, $params = array(), $cache = array())
    {
        $params = self::getParams($params, $route);
        return $this->client->request('GET', $this->baseUri.'/'.$route, [
            "query" => $params
        ], [], $cache);
    }

    /**
     * Post Entity With Custom Route
     *
     * @apiclient $client
     * @param string $route
     * @param array $params
     * @return array|WP_Error
     */
    public function postEntityWithCustomRoute($route, $params = array())
    {
        $params = self::getParams($params, $route);
        return $this->client->request('POST', $this->baseUri.'/'.$route, [
            "json" => $params
        ]);
    }

    /**
     * Create File into Entity - POST REQUEST
     *
     * @apiclient $client
     * @param string $entity
     * @param array $params
     * @return array|WP_Error
     */
    public function createFileEntity($ref, $entity, $params = array())
    {
        $route = $entity.'/';
        if (!is_int($ref)) {
            $ref = str_replace($this->baseUri.'/'.$route, '', $ref);
        }
        $route.= $ref;
        $params = self::getParams($params, $route);
        return $this->client->request('POST', $this->baseUri.'/'.$route.'/files', [
            'multipart' => $params
        ]);
    }

    /**
     * Get Entities collection
     *
     * @param string $entity
     * @param array $params
     * @param array $cache
     * @return array|WP_Error
     */
    public function getEntities($entity, array $params = array(), $cache = array())
    {
        $route = $entity;
        $params = self::getParams($params, $route);
        return $this->client->request('GET', $this->baseUri.'/'.$route, [
            "query" => $params
        ], [], $cache);
    }

    /**
     * Delete entity
     *
     * @param mixed $ref
     * @param string $entity
     * @return array|WP_Error
     */
    public function deleteEntity($ref, $entity)
    {
        $route = $entity.'/';
        if (!is_int($ref)) {
            $ref = str_replace($this->baseUri.'/'.$route, '', $ref);
        }
        $route.= $ref;
        return $this->client->request('DELETE', $this->baseUri.'/'.$route, []);
    }

    /**
     * Instanciate and get Front User Client
     *
     * @param array $params
     * @return ApiClient|WP_Error|null
     */
    public function getFrontUserClient($params = array(), $oauth = false)
    {
        if (null === $this->frontUserClient) {
            $keyConf = 'apisymfony';
            if (!defined('API_CONFIG') || !array_key_exists($keyConf, API_CONFIG)) {
                return null;
            }
            $country = $this->getCountry();
            $config = API_CONFIG[$keyConf][$country];
            if ($oauth) {
                $tokenAuthMode = ApiClient::TOKEN_GET_AUTHMODE_OAUTH;
                $tokenPath = $this->baseUri.'/oauth';
            } else {
                $tokenPath = $this->baseUri.'/auth';
                $tokenAuthMode = ApiClient::TOKEN_GET_AUTHMODE_JSON;
            }

            $options = [
                'username'         => ($params['username'] ?? ''),
                'password'         => ($params['password'] ?? ''),
                'tokenpath'        => $tokenPath,
                'tokenname'        => 'apisymfony-wp-client-frontenduser',
                'tokenmethod'      => ApiClient::TOKEN_METHOD_POST,
                'tokenbody'        => 'token',
                'tokengetauthmode' => $tokenAuthMode,
                'tokenauthmode'    => ApiClient::TOKEN_AUTH_BEARER,
                'tokencache'       => ApiClient::TOKEN_CACHE_SESSION,
                'token'            => ($params['token'] ?? null),
                'maxretries'       => 1,
                'debug'            => (array_key_exists('debug', $config) ? $config['debug'] : false),
            ];

            $guzzleOptions = [
                'base_uri'         => $config['api_url'],
            ];

            // Cache
            if (isset($config['cache'])) {
                $options['cache'] = $config['cache'];
            }
            $this->frontUserClient = new ApiClient($options, $guzzleOptions);
        }
        return $this->frontUserClient;
    }
}
