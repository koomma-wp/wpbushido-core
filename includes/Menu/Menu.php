<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Menu;

use WPBushido\Client\Client as WPBushidoClient;
use WPBushidoCore\Page\Page;

class Menu
{
    private $context = array();
    private $menuHeader = array();
    private $menuFooter = array();
    private $menuModules = array();
    private $menuItems = array();

    public function __construct(array $context)
    {
        $this->setContext($context);
    }

    /**
     * Set context (from Timber) property
     *
     * @param array $context
     * @return object
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Get context (from Timber) property
     *
     * @return array context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set menu header property
     *
     * @param array $menuHeader
     * @return object
     */
    public function setMenuHeader($menuHeader)
    {
        $this->menuHeader = $menuHeader;
        return $this;
    }

    /**
     * Get menu header property
     *
     * @return array context
     */
    public function getMenuHeader()
    {
        return $this->menuHeader;
    }

    /**
     * Set menu header property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInMenuHeader(string $key, $values)
    {
        $this->menuHeader[$key] = $values;
    }

    /**
     * Get menu header property sorted by key
     *
     * @return array context
     */
    public function getSortedMenuHeader()
    {
        ksort($this->menuHeader);
        return $this->menuHeader;
    }

    /**
     * Set menu footer property
     *
     * @param array $menuFooter
     * @return object
     */
    public function setMenuFooter($menuFooter)
    {
        $this->menuFooter = $menuFooter;
        return $this;
    }

    /**
     * Get menu footer property
     *
     * @return array context
     */
    public function getMenuFooter()
    {
        return $this->menuFooter;
    }

    /**
     * Get menu footer property sorted by key
     *
     * @return array context
     */
    public function getSortedMenuFooter()
    {
        ksort($this->menuFooter);
        return $this->menuFooter;
    }

    /**
     * Set menu footer property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInMenuFooter(string $key, $values)
    {
        $this->menuFooter[$key] = $values;
    }

    /**
     * Set menu modules property
     *
     * @param array $menuModules
     * @return object
     */
    public function setMenuModules($menuModules)
    {
        $this->menuModules = $menuModules;
        return $this;
    }

    /**
     * Get menu modules property
     *
     * @return array context
     */
    public function getMenuModules()
    {
        return $this->menuModules;
    }

    /**
     * Set menu modules property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInMenuModules(string $key, $values)
    {
        $this->menuModules[$key] = $values;
    }

    /**
     * Set menu items property
     *
     * @param array $menuItems
     * @return object
     */
    public function setMenuItems($menuItems)
    {
        $this->menuItems = $menuItems;
        return $this;
    }

    /**
     * Get menu items property
     *
     * @return array context
     */
    public function getMenuItems()
    {
        return $this->menuItems;
    }

    /**
     * Set menu items property in array data
     *
     * @param string $key
     * @param mixed $values
     * @return void
     */
    public function setInMenuItems(string $key, $values)
    {
        $this->menuItems[$key] = $values;
    }

    /**
     * Get all menu data
     *
     * @return array complete menu data
     */
    public function getMenu()
    {
        $menu = array();
        $this->getMenuPages();
        foreach ($this->getMenuItems() as $page) {
            if ($page['parent'] == 0 && $page['header']) {
                $page = $this->getMenuChilds($page, 'header');
                $this->setInMenuHeader($page['header'].'-'.$page['page']->ID, $page);
            }
            if ($page['parent'] == 0 && $page['footer']) {
                $page = $this->getMenuChilds($page, 'footer');
                $this->setInMenuFooter($page['footer'].'-'.$page['page']->ID, $page);
            }
        }
        foreach ($this->getMenuModules() as $module => $moduleItem) {
            if (isset($this->getMenuItems()[$moduleItem])) {
                $this->setInMenuModules($module, $this->getMenuItems()[$moduleItem]);
            }
        }

        //ksort($this->getMenuHeader());
        $menu['header'] = $this->getSortedMenuHeader();
        $menu['footer'] = $this->getSortedMenuFooter();
        $menu['modules'] = $this->getMenuModules();
        $menu['items'] = $this->getMenuItems();
        return $menu;
    }

    /**
     * Get recursive page childs data
     *
     * @param array $page
     * @param string $type (header or footer)
     * @return array
     */
    public function getMenuChilds($page, $type)
    {
        if (count($page['childs'])) {
            $childs = $page['childs'];
            $page['childs'] = array();
            foreach ($childs as $key => $child) {
                if (isset($this->getMenuItems()[$child])) {
                    $childPage = $this->getMenuItems()[$child];
                    if ($childPage[$type]) {
                        $page['childs'][$childPage[$type].'-'.$childPage['page']->ID] = $this->getMenuChilds($childPage, $type);
                    }
                }
            }
            ksort($page['childs']);
        }
        return $page;
    }


    /**
     * Build menu from WP Page
     *
     * @param int $parent
     * @return void
     */
    public function getMenuPages($parent = 0)
    {
        // Get Main Page
        $context = $this->getContext();
        $args = array(
            'sort_order' => 'asc',
            'post_parent' => $parent,
            'post_type' => 'page',
            'numberposts' => -1,
            'post_status' => 'publish',
        );
        $pages = get_posts($args);
        foreach ($pages as $pageK => $page) {
            $pageFields = get_fields($page->ID);
            $activated = false;
            $activatedChild = false;
            if (isset($context['page_id']) && $page->ID == $context['page_id']) {
                $activated = true;
            }
            $tplName = WPBushidoClient::getTplName($page->ID);
            $urlPage = get_permalink($page->ID);
            $pageObject = new Page(array());
            $pageMenuItem = array(
                'page' => new \Timber\Post($page->ID),
                'page_acf' => $pageFields,
                'subpages' => array(),
                'url' => $urlPage,
                'active' => $activated,
                'childs' => array(),
                'header' => false,
                'footer' => false,
                'parent' => $parent
            );
            $pageMenuItem['subpages'] = $pageObject->getSubPages($pageMenuItem);

            // Header
            if (isset($pageFields['header']) && !empty($pageFields['header'])) {
                $pageMenuItem['header'] = intval($pageFields['header']);
            }

            // Footer
            if (isset($pageFields['footer']) && !empty($pageFields['footer'])) {
                $pageMenuItem['footer'] = intval($pageFields['footer']);
            }

            if ($parent) {
                if (isset($this->getMenuItems()[$parent])) {
                    $pageParentItem = $this->getMenuItems()[$parent];
                    $pageParentItem['childs'][] = $page->ID;
                    $this->setInMenuItems($parent, $pageParentItem);
                }
            }

            // Modules
            if ($tplName && !isset($this->getMenuModules()[$tplName])) {
                $this->setInMenuModules($tplName, $page->ID);
            }
            // Items
            $this->setInMenuItems($page->ID, $pageMenuItem);

            // Childs
            $this->getMenuPages($page->ID);
        }
    }

    /**
     * Browse all pages controller and set route / url rewrite
     *
     * @return void
     */
    public function getPagesRouting()
    {
        if ($handle = opendir(plugin_dir_path(__FILE__). '../Page')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $namePageClass = 'WPBushidoCore\\Page\\' . str_replace(array('-', '_', '.php'), array('', '', ''), $entry);
                    if (class_exists($namePageClass) && $namePageClass != 'WPBushidoCore\Page\Page') {
                        $page = new $namePageClass(array());
                        add_filter('query_vars', array($page, 'addPageVars'), 0, 1);
                        $page->setPagesVarsRules();
                    }
                }
            }
            closedir($handle);
        }
    }
}