<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Security;

use WPBushidoCore\Entity\User;

class Settings
{
    /**
     * Setup Security settings methods
     *
     * @return void
     */
    public static function setup()
    {
        show_admin_bar(false);
        add_filter('authenticate', array(self::class, 'apiAuthentificate'), 10, 3 );
        add_action('send_headers', array(self::class, 'forceHeader'), 99);
        remove_action( 'authenticate', 'wp_authenticate_username_password', 20);
        remove_action( 'authenticate', 'wp_authenticate_email_password', 20);
        add_action('authenticate', array(self::class, 'apiAuthentificateUsernamePassword'), 20, 3);
        add_action('authenticate', array(self::class, 'apiAuthentificateEmailPassword'), 20, 3);
        add_action('admin_init', array(self::class, 'disableCommentsSupport'));
        add_filter('comments_open', array(self::class, 'disableCommentsStatus'), 20, 2);
        add_filter('pings_open', array(self::class, 'disableCommentsStatus'), 20, 2);
        add_filter('comments_array', array(self::class, 'disableCommentsHideExistingComments'), 10, 2);
        add_action('admin_menu', array(self::class, 'removeAdminCommentsMenu'));
        add_action('admin_init', array(self::class, 'redirectAdminCommentsMenu'));
        add_action('admin_init', array(self::class, 'disableDashboardMetabox'));
        add_action('admin_init', array(self::class, 'hideMenuPages'));
        add_action('init', array(self::class, 'disableCommentsAdminBar'));
        add_action('init', array(self::class, 'disableAdminForSubscriber'));
        #add_action('init', array(self::class, 'wpStartSession'), 1);
        #add_action('wp_logout', array(self::class, 'wpEndSession'));
        #add_action('wp_login', array(self::class, 'wpEndSession'));
    }

    /**
     * Disable comments support in admin
     *
     * @return void
     */
    public static function disableCommentsSupport()
    {
        $post_types = get_post_types();
        foreach ($post_types as $post_type) {
            if(post_type_supports($post_type, 'comments')) {
                remove_post_type_support($post_type, 'comments');
                remove_post_type_support($post_type, 'trackbacks');
            }
        }
    }

    /**
     * Disable comments status
     *
     * @return boolean
     */
    public static function disableCommentsStatus()
    {
        return false;
    }

    /**
     * Disable comments status
     *
     * @return array
     */
    public static function disableCommentsHideExistingComments()
    {
        $comments = array();
        return $comments;
    }

    /**
     * Remove Admin Comments Menu
     *
     * @return void
     */
    public static function removeAdminCommentsMenu()
    {
        remove_menu_page('edit-comments.php');
    }

    /**
     * Redirect comment admin page
     *
     * @return void
     */
    public static function redirectAdminCommentsMenu()
    {
        global $pagenow;
        if ($pagenow === 'edit-comments.php') {
            wp_redirect(admin_url()); exit;
        }
    }

    /**
     * Remove Dashboard metabox
     *
     * @return void
     */
    public static function disableDashboardMetabox()
    {
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
    }

    /**
     * Disable Comments Admin Bar
     *
     * @return void
     */
    public static function disableCommentsAdminBar()
    {
        if (is_admin_bar_showing()) {
            remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
        }
    }


    public static function apiAuthentificate($user, $email, $password)
    {
        if ($email == "" || $password == "") return;
        $checkLogin = array("username" => $email, "password" => $password);
        $userObj = new User(false);
        $userObj->setContext(array('options' => array('country' => 'FR', 'lang' => 'fr')));
        $auth = $userObj->getApiAuthUser($checkLogin);
        if (!is_wp_error($auth) && isset($auth['@id']) && !empty($auth['@id'])) {
            $userobj = new \WP_User();
            $loginId = strtolower(str_replace('/', '', $auth['@id']));
            $user = $userobj->get_data_by('login', $loginId);
            if ($user->ID == 0) {
                $userdata = array(
                    'user_login' => $auth['@id'],
                    'user_email' => sha1($auth['@id']).'@koomma.fr',
                    'display_name' => $auth['@id'],
                );
                if (in_array('ROLE_SUPER_ADMIN', $auth['roles']) || in_array('ROLE_ADMIN', $auth['roles'])) {
                    $userdata['role'] = 'administrator';
                }
                $new_user_id = wp_insert_user($userdata);
                $user = new \WP_User($new_user_id);
            } else {
                $user = new \WP_User($user->ID);
            }
            return $user;
        } else {
            return;
        }
    }

    public static function apiAuthentificateUsernamePassword($user, $username, $password)
    {
        if ($user instanceof \WP_User) {
            return $user;
        }
        // Validations and error messages
        return new \WP_Error('broke', __( "I've fallen and can't get up"));
    }

    public static function apiAuthentificateEmailPassword($user, $email, $password)
    {
        if ($user instanceof \WP_User) {
            return $user;
        }
        // Validations and error messages
        return new \WP_Error('broke', __( "I've fallen and can't get up"));
    }

    public static function disableAdminForSubscriber()
    {
        if (is_admin() && ! current_user_can('subscriber' ) && !(defined('DOING_AJAX') && DOING_AJAX ) ) {
            wp_redirect( home_url() );
        }
    }

    public static function hideMenuPages()
    {
        //remove_menu_page('users.php');
    }

    public static function forceHeader()
    {
        if (!is_admin()) {
            $options = get_fields('option');
            $noResetCache = false;
            $private = false;
            if (isset($_SERVER['REQUEST_URI'])) {
                $uri = $_SERVER['REQUEST_URI'];
                if (isset($options['uncache_template']) && !empty($options['uncache_template'])) {
                    foreach ($options['uncache_template'] as $page) {
                        if (strstr($uri, $page['page'])) {
                            $noResetCache = true;
                            if (isset($page['private']) && $page['private'] === true) {
                                $private = true;
                            }
                        }
                    }
                }
            }
            if (!$noResetCache && !is_user_logged_in()) {
                header_remove('Cache-Control');
                header_remove('Pragma');
                header_remove('Expires');
                if ($private) {
                    header('Cache-Control: max-age=0, private, s-maxage=0, no-cache, no-store, must-revalidate');
                } else {
                    header('Cache-Control: max-age=1800, public, s-maxage=1800');
                }
            }
        }
    }
    function wpStartSession()
    {
        if(!session_id()) {
            session_start();
        }
    }

    function wpEndSession()
    {
        if(session_id()) {
            session_destroy();
        }
    }
}
