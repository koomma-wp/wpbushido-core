<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Client;

use WPBushidoCore\Page\Page;

class Client extends \WPBushido\Client\Client
{

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get View / Twig context , apply and return specific modification
     *
     * @param array $context
     * @return array modified context
     */
    public function addToContext($context) {
        $context = parent::addToContext($context);
        // Get ENV
        $context['env'] = $_ENV;
        if (!is_admin()) {
            $pageId = get_queried_object_id();
            if (!$pageId) {
                $pageId = intval(get_option('page_on_front'));
                $context['page_id'] = $pageId;
                $context['page_object'] = new \Timber\Post($pageId);
                $context['page_acf'] = get_fields($pageId);
                $context['error_404'] = true;
            }
            $context['page_template'] = $context['page_object']->_wp_page_template;
            $context['page_template'] = str_replace(array('controller/', '.php'), array('', ''), $context['page_template']);
        }
        return $context;
    }

    /**
     * Add to twig / view  Leoo function and filters
     *
     * @param object $twig
     * @return object twig
     */
    public function addToTwig($twig)
    {
        $twig = parent::addToTwig($twig);
        $twig->addExtension(new \Twig_Extensions_Extension_Intl());
        $twig->addFunction(new \Timber\Twig_Function('getProductUrl', array($this, 'getProductUrl')));
        $twig->addFunction(new \Timber\Twig_Function('checkPageDisplay', array($this, 'checkPageDisplay')));
        $twig->addFilter(new \Twig_SimpleFilter('cleanContent', array($this, 'cleanContent')));
        $twig->addFilter(new \Twig_SimpleFilter('replaceDynamicData', array($this, 'replaceDynamicData')));
        return $twig;
    }

    /**
     * Twig function for Akeneo Product from Wordpress Product Article to get good slug
     *
     * @param string $url
     * @return string $slug
     */
    public function getProductUrl($url)
    {
        $url = str_replace(home_url().'/', '', $url);
        $url = str_replace('/', '', $url);
        $post = get_page_by_path($url,OBJECT,'product');
        if (isset($post->ID)) {
            $externalId = get_field('external_id', $post->ID);
            return str_replace('/api/akeneo/products/', '', $externalId);
        } else {
            return '';
        }
    }

    /**
     * Twig filter to clean html content before display
     *
     * @param string $content
     * @return string $newcontent
     */
    public function cleanContent($content)
    {
        $newcontent = preg_replace("/<p[^>]*?>/", "", $content);
        $newcontent = str_replace("</p>", "<br />", $newcontent);
        $newcontent = $this->replaceDynamicData($newcontent);
        return $newcontent;
    }

    /**
     * Twig function to replace # stppword by context dynamic data
     *
     * @param string $string
     * @return string $string
     */
    public function replaceDynamicData($string)
    {
        return \WPBushido\Helpers\Data::replaceDynamicData($string, \Timber::get_context());
    }

    /**
     * Check page display with ACF field condition display
     *
     * @param array $page
     * @return boolean
     */
    public function checkPageDisplay($page)
    {
        $return = true;
        $context = \Timber::get_context();
        if (isset($page['page_acf']['condition_display']) && !empty($page['page_acf']['condition_display'])) {
            $return = false;
            $conditions = explode(';', $page['page_acf']['condition_display']);
            $success = 0;
            foreach ($conditions as $condition) {
                $conditionElements = explode('|', $condition);
                if (count($conditionElements) == 3) {
                    $element = \WPBushido\Helpers\Data::findInContextValue($conditionElements[0], $context);
                    $toTest = $conditionElements[2];
                    if ($toTest == 'false' || $toTest == 'true') {
                        $toTest = ($toTest === 'true');
                    }
                    if ($conditionElements[1] == '=') {
                        if (\WPBushido\Helpers\Data::conditionEqual($element, $toTest)) {
                            $return = true;
                        }
                    }
                }
            }
        }
        return $return;
    }


}
