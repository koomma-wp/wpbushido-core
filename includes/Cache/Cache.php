<?php
/**
 * WPBushidoCore Plugin
 *
 * @package WPBushidoCore
 */

namespace WPBushidoCore\Cache;

use Cache\Adapter\Redis;

class Cache
{
    /**
     * Cache Config
     *
     * @var self
     */
    private static $config = false;

    /**
     * Current Cache instance
     *
     * @var self
     */
    private static $_instance = null;


    private static $defaultTtl = 3600;

    /**
     * Register Hooks of Cache. To execute in "init" action hook
     *
     * @return void
     */

    public function __construct($config)
    {
        self::$config = $config;
    }

    public static function instance($config)
    {
        if (self::$_instance == null) {
            self::$_instance = new self($config);
        }
        return self::$_instance;
    }

    public static function getCache($key)
    {
        $cache = false;
        if (self::$config) {
            $key = 'cachewpclient_' . sha1($key);
            if (isset(self::$config['type']) && self::$config['type'] == 'redis') {
                if (array_key_exists('mode', self::$config) && self::$config['mode'] == 'cluster') {
                    $client = new \RedisCluster(null, [self::$config['host'] .":". self::$config['port']]);
                }
                else {
                    $client = new \Redis();
                    $client->connect(self::$config['host'], self::$config['port']);
                }
                $pool = new \Cache\Adapter\Redis\RedisCachePool($client);
                $cacheObj = $pool->getItem($key);
                $findCache = $pool->hasItem($key);
                if ($findCache && $cacheObj->isHit()) {
                    $cache = self::unCompressCache($cacheObj->get());
                }
            }
        }
        return $cache;
    }

    public static function setCache($key, $data, $ttl = false)
    {
        $cachedData = false;
        if (self::$config) {
            $key = 'cachewpclient_' . sha1($key);
            if (isset(self::$config['type']) && self::$config['type'] == 'redis') {
                if (array_key_exists('mode', self::$config) && self::$config['mode'] == 'cluster') {
                    $client = new \RedisCluster(null, [self::$config['host'] .":". self::$config['port']]);
                }
                else {
                    $client = new \Redis();
                    $client->connect(self::$config['host'], self::$config['port']);
                }
                $pool = new \Cache\Adapter\Redis\RedisCachePool($client);
                $cacheObj = $pool->getItem($key);
                $findCache = $pool->hasItem($key);
                $createCache = true;
                if ($findCache) {
                    if ($cacheObj->isHit()) {
                        $createCache = false;
                    }
                }
                if ($createCache) {
                    $cacheObj->set(self::compressCache($data));
                    if ($ttl) {
                        $cacheObj->expiresAfter($ttl);
                    }
                    else {
                        $cacheObj->expiresAfter(self::$defaultTtl);
                    }
                    $pool->save($cacheObj);
                    $cachedData = true;
                }
            }
        }
        return $cachedData;
    }

    public static function compressCache($data)
    {
        return gzcompress(serialize($data));
    }

    public static function unCompressCache($data)
    {
        return unserialize(gzuncompress($data));
    }
}
