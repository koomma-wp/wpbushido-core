<?php
/**
 * WPBushidoCore
 *
 * @package   WPBushidoCore
 * @author    Philippe AUTIER <philippe.autier@koomma.fr>
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name:    	  WPBushidoCore
 * Description:       Wordpress Leoo Core Bundle
 * Version:           1.0.2
 * Author:            BUSHIDO Dev Team
 * Author URI:        https://www.koomma.fr
 * Text Domain:       wpbushido-core
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// Exit if accessed directly

if (!defined('ABSPATH')) {
    exit;
}

if (!defined('BUSHIDO_WP_CORE_PLUGIN_URL')) {
    define('BUSHIDO_WP_CORE_PLUGIN_URL', plugin_dir_url(__FILE__));
}

if (!defined('BUSHIDO_WP_CORE_PLUGIN_PATH')) {
    define('BUSHIDO_WP_CORE_PLUGIN_PATH', plugin_dir_path(__FILE__));
}

if (!defined('WPTM_LANGUAGE_DOMAIN')) {
    define('WPTM_LANGUAGE_DOMAIN', 'bushidocore');
}

spl_autoload_register(function($class) {
    $namespaces = explode('\\', $class);
    $project = reset($namespaces);

    if ($project == 'WPBushidoCore') {
        unset($namespaces[0]);
        $path = null;
        while (($namespace = current($namespaces)) !== false) {
            $path .= DIRECTORY_SEPARATOR . $namespace;
            next($namespaces);
        }
        $path .= '.php';

        if (file_exists(plugin_dir_path(__FILE__) . 'includes' . $path)) {
            require (plugin_dir_path(__FILE__) . 'includes' .  $path);
        }
    }
});

add_action('init', function() {

    if (defined('Inpsyde\Wonolog\LOG')) {
        Inpsyde\Wonolog\bootstrap();
    }
    // Router
    \WPBushidoCore\Router\Router::registerHooks();
    // Router :: Incoming Url Args
    $menu = new \WPBushidoCore\Menu\Menu(array());
    $menu->getPagesRouting();

    \WPBushidoCore\Cron\Task\AkeneoPimProductToWp::registerHooks();
});
